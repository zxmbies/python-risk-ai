def get_match(hand):
    '''iterates through hand and auto-finds a match. Only to be
    called when there are the max (6 as of 08/25/2013) number of
    cards in the hand. Algorithm is O(max choose 3)''' 
    for i in range(len(hand)):
        card1 = hand[i]
        for j in range(i+1, len(hand)):
            card2 = hand[j]
            for k in range(j+1, len(hand)):
                card3 = hand[k]
                if ((card1.card_type is card2.card_type and card1.card_type is card3.card_type)
                        or (card1.card_type is 0) or (card2.card_type is 0) or (card3.card_type is 0)
                        or ((not card1.card_type is card2.card_type)
                            and (not card1.card_type is card2.card_type)
                            and (not card2.card_type is card3.card_type))):
                    return card1, card2, card3
 