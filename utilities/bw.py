import model.player
import model.map
import model.deck
import model.game

NUMBER_OF_CARDS = 44
NUMBER_OF_TERRITORIES = 42
CARD_TYPES = 3 #does not include wild

def build_world(username):
    
    '''Takes the first user, and adds them/neutral,
    calls helper function bmff to construct map, compiles
    deck, creates game from above data.'''
    
    players_temp = ()
    players_temp += (model.player.Player(0),)
    players_temp[0].eliminated = True
    players_temp += (model.player.Player(0, 1, username),)
    
    game_map = model.map.Map(get_string_from_file("mapstring.data"))
    cards_temp = ()
    for node_id in range(0,NUMBER_OF_TERRITORIES, CARD_TYPES):
        for inc in range(CARD_TYPES):
            cards_temp += (model.deck.Card(node_id + inc, inc + 1),)
    cards_temp += (model.deck.Card(-1,0), )
    cards_temp += (model.deck.Card(-1,0), )
    
    game_deck = model.deck.Deck()
    for card in range(NUMBER_OF_CARDS):
        game_deck.cards.append(cards_temp[card])

    built_game = model.game.Game(game_map, game_deck, players_temp)
    return built_game

def get_string_from_file(filename):
    file = open(filename, "r")
    line = file.readline()
    return line