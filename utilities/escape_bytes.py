import sys
import binascii

def escape_bytes(value):
    return "x'%s'" % binascii.hexlify(value).decode(sys.getdefaultencoding())
