def check_match(card1, card2, card3):
    
    '''Checks match, according to risk official rules, in shortest way possible'''
    if ((card1.card_type == card2.card_type and card1.card_type == card3.card_type)
        or (card1.card_type == 0) or (card2.card_type == 0) or (card3.card_type == 0)
        or ((not card1.card_type == card2.card_type)
            and (not card1.card_type == card3.card_type)
            and (not card2.card_type == card3.card_type))):
        return True

    return False
