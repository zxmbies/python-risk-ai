var Address = "ws://lenin.duckdns.org:8080/message";

if ("WebSocket" in window)
{
	var ws = new WebSocket(Address);
	ws.onmessage = function(evt)
	{
		var received_msg = JSON.parse(evt.data);
		switch (received_msg.message_type) {
		
			case "send_username":
				local_player.set({ username : received_msg.username })
				break;
			case "new_game":
				lobby.reset();
				for (person in (received_msg.players))
				{
					if (received_msg.players[person].player_name == local_player.get("username"))
					{
						local_player.set({"number" : received_msg.players[person].player_id});
						game.add(local_player);
					}
					else
					{
						var temp_player = new Player({ username : received_msg.players[person].player_name,
													   number : received_msg.players[person].player_id
													 });
						game.add(temp_player);
					}
				}
				for (node in received_msg.map.nodes)
				{
					temp_node = new Node({ name : received_msg.map.nodes[node].name,
										   number : node,
										   units: received_msg.map.nodes[node].num_ships,
										 });
					node_player = game.findWhere({number : received_msg.map.nodes[node].player.player_id});
					temp_node.set({player : node_player});
					nodes.add(temp_node);
				}
				break;
			case "begin_turn":
				if (received_msg.player_id == local_player.get("number"))
				{
					local_player.set({units : received_msg.players[local_player.get("number")].reserved_ships});
					game.findWhere({number: 0}).set({units : received_msg.players[0].reserved_ships});
					var start_timer = {
						"message_type" : "timer_start"
					};
					ws.send	(JSON.stringify(start_timer));
				}
				
				player_temp = game.findWhere({number : received_msg.player_id});

				turnTimer = new TurnTimer({player_turn : player_temp,
										   turn_timer : 0 //THIS NEEDS TO BE IMPLEMENTED!!!! LOOK HERE!!!! HEY!!!! LISTEN!!!!
										   });
				
				for (player in received_msg.players)
				{
					var player_temp = game.findWhere({number : player.player_id});
					var cards = player_temp.get("cards");
	
					while (cards.length < player.num_cards)
					{
						temp_card = new Card();
						cards.add(temp_card);
					}
					while (cards.length > player.num_cards)
					{
						cards.pop();
					}
				}
				break;
			case "place_ships":
				var place_node = nodes.findWhere({number : received_msg.node_id});
				var neutral = game.findWhere({number : 0})
				
				place_node.set({number : place_node.units + received_msg.num_ships});
				if (place_node.player == local_player)
				{
					local_player.set({units : local_player.units - received_msg.num_ships});
				}
				else if (place_node.player == neutral && neutral.get("units") > 0)
				{
					neutral.set({units : neutral.units - received_msg.num_ships});
				}
				
				actions = new Actions({place : function () { new Place({
															node : place_node,
															number : received_msg.num_ships
															});
															}
														});
				break;
			case "declare_attack":
				var attacking_node_temp = nodes.findWhere({number : received_msg.start_node});
				var defending_node_temp = nodes.findWhere({numger : received_msg.end_node});
				var attack_dice_temp = received_msg.num_dice;
				var attacking_player_temp = attacking_node_temp.get("player")
				var defending_player_temp = defending_node_temp.get("player")
				
				actions = new Actions({attack : function() {new Attack ({
									attacking_node : attacking_node_temp,
									defending_node : defending_node_temp,
									attack_dice : attack_dice_temp,
									attacking_player : attacking_player_temp,
									defending_player : defending_player_temp
									});
									}
								});
				break;
			case "resolve_attack":
				var defending_dice_temp = received_msg.num_dice;
				var attacker_losses_temp = received_msg.attacker_losses;
				var defender_losses_temp = received_msg.defender_losses;
				var attack_temp = actions.get("attack");
				var attacking_node_temp = attack_temp.get("attacking_node");
				var defending_node_temp = attack_temp.get("defending_node");
				var attacking_node_units = attacking_node_temp.get("units");
				var defending_node_units = defending_node_temp.get("units");
				
				attack_temp.set({defending_dice : defending_dice_temp,
								 attacker_losses : attacker_losses_temp,
								 defender_losses : defender_losses_temp});
				
				attacking_node_temp.set({units : attacking_node_units - attacker_losses_temp});
				defending_node_temp.set({units : dfending_node_units - defender_losses_temp});
				break;
			case "move_ships":
				var start_node_temp = nodes.findWhere({number : received_msg.start_node})
				var end_node_temp = nodes.findWhere({number : received_msg.end_node})
				var start_node_units = start_node_temp.get("units")
				var end_node_units = end_node_temp.get("units")
				
				actions = new Actions({move : function () {new Move ({
									start_node : start_node_temp,
									end_node : end_node_temp,
									number : received_msg.num_ships
									});
									}
								});
								
				start_node_temp.set({units : start_node_units - received_msg.num_ships})
				end_node_temp.set({units : end_node_units + received_msg.num_ships})
				break;
			case "draw_card":
				var node_temp = nodes.findWhere({number : received_msg.card.node_id});

				var card_temp = new Card({node : node_temp,
										  type : received_msg.card.card_type
										  });
			
				local_player.cards.add(card_temp);
				break;
			case "play_cards":
				var player_units = local_player.get("units");
				
				local_player.set({units : player_units + received_msg.num_ships});
				break;
			case "remove_card":
				var hand_temp = local_player.get("cards");
				var node_temp = game.findWhere({number : received_msg.card.node_id});
				var card_temp = hand_temp.findWhere({node : node_temp});			

				hand_temp.remove(card_temp);
				break;
			case "lobby_connect":
				var player_temp = new Player({username : received_msg.username});
				
				lobby.add(player_temp);
				break;
			case "lobby_disconnect":
				var player_temp = lobby.findWhere({username : received_msg.username});
				
				lobby.remove(player_temp);
			case "create_lobby":
				for (username_temp in received_msg.usernames)
				{
					var player_temp = new Player({username : username_temp});
					
					lobby.add(player_temp);
				}
				break;
			case "challenge_sent":
				actions = new Actions({challenge : function () { new Challenge({
																	challenger : received_msg.username,
																	challengee : received_msg.opponent
																	});
																}
									  });
				break;
			case "game_won":
				game.reset();
				nodes.reset();
				
				actions = new Actions({gameWon : function () { new GameWon ({
																			winnner : received_msg.username
																			});
									   						 }
									  });									
				break;
			case "end_turn":
				actions = new Actions({endTurn : true});
				break;
			case "disconnected_user":
				var temp_player = game.findWhere({username : received_msg.username});
				
				disconnectedPlayers.add(temp_player);
				break;
			case "player_reconnect":
				var temp_player = disconnectedPlayers.findWhere({username : received_msg.username});
				
				disconnectedPlayers.remove(temp_player);
				break;
		}
	}
}
function LobbyConnect("username")
{
    
}
function SendChallenge(challengee_name)
{
	var challenge_msg = {
		"message_type" : "challenge_user",
		"username" : challengee_name
	};
	
	ws.send(JSON.stringify(challenge_msg));
}
function SendJoinQueue()
{
	var join_queue_msg = {
		"message_type" : "join_queue"
	};
	
	ws.send(JSON.stringify(join_queue_msg));
}
function SendPlace(units, node_number)
{
	var place_msg = {
		"message_type" : "place_ships",
		"num_ships" : units,
		"node_id" : node_number
	};
	
	ws.send(JSON.stringify(place_msg));
}
function SendAttack(number_of_dice, attacking_node_number, defending_node_number)
{
	var attack_msg = {
		"message_type" : "declare_attack",
		"num_dice" : number_of_dice,
		"start_node" : attacking_node_number,
		"end_node" : defending_node_number
	};
	
	ws.send(JSON.stringify(attack_msg));
}
function SendMove(number_of_units, starting_node_number, ending_node_number)
{
	var move_msg = {
		"message_type" : "move_ships",
		"num_ships" : number_of_units,
		"start_node" : starting_node_number,
		"end_node" : ending_node_number
	};
		
	ws.send(JSON.stringify(move_msg));
}
function SendPlayCards(card1, card2, card3)
{
	var node1 = card1.get("node");
	var node2 = card2.get("node");
	var node3 = card3.get("node");

	var card1_object = {
		"node_id" : node1.get("number"),
		"card_type" : card1.get("type")
	};
	var card2_object = {
		"node_id" : node2.get("number"),
		"card_type" : card2.get("type")
	};
	var card3_object = {
		"node_id" : node3.get("number"),
		"card_type" : card3.get("type")
	};
	
	card1_string = JSON.stringify(card1_object);
	card2_string = JSON.stringify(card2_object);
	card3_string = JSON.stringify(card3_object);

	var cards = 
	{
		"card1" : card1_string,
		"card2" : card2_string,
		"card3" : card3_string
	};
	
	var cards_string = JSON.stringify(cards);
	
	var play_cards_msg = {
		"message_type" : "play_cards",
		"cards" : cards_string
	};
	
	ws.send(JSON.stringify(play_cards_msg));
}
function SendDefense(number_of_dice)
{
	var defense_msg = {
		"message_type" : "defend_roll",
		"num_dice" : number_of_dice
	};
	
	ws.send(JSON.stringify(defense_msg));
}
function SendEndTurn()
{
	var end_turn_msg = {
		"message_type" : "end_turn"
	};
	
	ws.send(JSON.stringify(end_turn_msg));
}
function SendForfeit()
{
	var forfeit_msg	= {
		"message_type" : "forfeit"
	};
	
	ws.send(JSON.stringify(forfeit_msg));
}