function draw(canv) {
    var context = canv.getContext("2d");
    context.fillStyle = "#FFFFFF";
    context.strokeStyle = "#0000FF";
    drawCircle(context, 533, 363, 12);
}

function drawCircle(ctx, x, y, r) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2*Math.PI);
    ctx.fill();
    ctx.stroke();
}

function drawLine(ctx, x1, y1, x2, y2) {
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}