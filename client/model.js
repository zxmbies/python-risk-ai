var Card = Backbone.Model.extend({
	
	defaults : {
		node : null,
		type : null
	}
});

var Hand = Backbone.Model.extend({
	model : Card
});

var Player = Backbone.Model.extend({

	defaults : {
		username : "",
		number : null,
		cards : function() { new Hand({}); },
		units: 0,
		disconnect_time: 300
	}
});

var Node = Backbone.Model.extend({

	defaults : {
		name : null,
		number : null,
		units : 0,
		player : null
	}
});

var Nodes = Backbone.Collection.extend({
	model : Node,
});

var Lobby = Backbone.Collection.extend({
	model : Player,
});

var Game = Backbone.Collection.extend({
	model : Player,
});

var Attack = Backbone.Model.extend({
	
	defaults : {
		attacking_player : null,
		defending_player : null,
		attacking_node : null,
		defending_node : null,
		attack_dice : null,
		defend_dice : null,
		attacker_losses : null,
		defender_losses : null
	}
});

var Move = Backbone.Model.extend({

	defaults : {
		start_node : null,
		end_node : null,
		number : null
	}
});

var Place = Backbone.Model.extend({

	defaults : {
		node : null,
		number : null
	}
});

var PlayCards = Backbone.Model.extend({

	defaults : {
		player : null,
		card1 : null,
		card2 : null,
		card3 : null
	}
});

var Challenge = Backbone.Model.extend({
	
	defaults : {
		challenger : null,
		challengee : null
	}
});

var GameWon = Backbone.Model.extend({
	
	defaults : {
		winner : null
	}
});

var PlanAttack = Backbone.Model.extend({

	defaults : {
		attacking_node : null,
		defending_node : null
	}
});

var PlanMove = Backbone.Model.extend({

	defaults : {
		starting_node : null,
		defending_node: null
	}
});

var PlanPlace = Backbone.Model.extend({

	defaults : {
		node : null
	}
});
var Actions = Backbone.Model.extend({

	defaults : {
		attack : null,
		move : null,
		place : null,
		challenge : null,
		gameWon : null,
		planAttack : null,
		planMove : null,
		planPlace : null,
		endTurn : null
	}
});

var TurnTimer = Backbone.Model.extend({

	defaults : {
		player_turn : null,
		time : null,
		pause : false
	}
});

var DisconnectTimer = Backbone.Model.extend({
	
	defaults : {
		player : null,
		time : null
	}
});

var DisconnectedPlayers = Backbone.Model.extend({
	model : Player
});

var LocalPlayerCollect = Backbone.Model.extend({
	model : Player
});

var local_player = new Player({});
var localPlayerCollect = new LocalPlayerCollect(local_player);
var game = new Game;
var nodes = new Nodes;
var lobby = new Lobby;
var actions = null;
var turnTimer = new TurnTimer;
var disconnectedPlayers = new DisconnectedPlayers;