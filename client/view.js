var NodeView = Backbone.View.extend({

    //TODO:
    //Write a view class for the Nodes (contains all map nodes) container
    nodeData: [
               {node_id:0,area:0,connections:[1,3,29],name:"Alaska",coords:[67,61]},
               {node_id:1,area:0,connections:[0,2,3,4],name:"Northwest Territory",coords:[147,55]},
               {node_id:2,area:0,connections:[1,4,5,13],name:"Greenland",coords:[352,34]},
               {node_id:3,area:0,connections:[0,1,4,6],name:"Alberta",coords:[117,97]},
               {node_id:4,area:0,connections:[1,2,3,5,6,7],name:"Ontario",coords:[191,102]},
               {node_id:5,area:0,connections:[2,4,7],name:"Quebec",coords:[251,99]},
               {node_id:6,area:0,connections:[3,4,7,8],name:"Western United States",coords:[104,146]},
               {node_id:7,area:0,connections:[4,5,6,8],name:"Eastern United States",coords:[172,154]},
               {node_id:8,area:0,connections:[6,7,9],name:"Central America",coords:[115,208]},
               {node_id:9,area:1,connections:[8,10,11],name:"Venezuela",coords:[220,273]},
               {node_id:10,area:1,connections:[9,11,12],name:"Peru",coords:[233,356]},
               {node_id:11,area:1,connections:[9,10,12,20],name:"Brazil",coords:[281,334]},
               {node_id:12,area:1,connections:[10,11],name:"Argentina",coords:[244,415]},
               {node_id:13,area:2,connections:[2,14,15],name:"Iceland",coords:[448,66]},
               {node_id:14,area:2,connections:[13,15,16,18],name:"Great Britain",coords:[434,95]},
               {node_id:15,area:2,connections:[13,14,16,17],name:"Scandinavia",coords:[500,69]},
               {node_id:16,area:2,connections:[14,15,17,18,19],name:"Northern Europe",coords:[497,105]},
               {node_id:17,area:2,connections:[15,16,19,26,30,35],name:"Ukraine",coords:[558,93]},
               {node_id:18,area:2,connections:[14,16,19,20],name:"Western Europe",coords:[454,135]},
               {node_id:19,area:2,connections:[16,18,20,21,35],name:"Southern Europe",coords:[517,137]},
               {node_id:20,area:3,connections:[11,18,19,21,22,23],name:"North Africa",coords:[450,230]},
               {node_id:21,area:3,connections:[19,20,22,35],name:"Egypt",coords:[522,198]},
               {node_id:22,area:3,connections:[20,21,23,24,25,35],name:"East Africa",coords:[588,270]},
               {node_id:23,area:3,connections:[20,22,24],name:"Congo",coords:[522,300]},
               {node_id:24,area:3,connections:[22,23,25],name:"SouthAfrica",coords:[533,363]},
               {node_id:25,area:3,connections:[22,24],name:"Madagascar",coords:[607,368]},
               {node_id:26,area:4,connections:[17,27,30,34],name:"Ural",coords:[649,78]},
               {node_id:27,area:4,connections:[26,28,31,32,34],name:"Siberia",coords:[708,65]},
               {node_id:28,area:4,connections:[27,29,31],name:"Yakutsk",coords:[785,61]},
               {node_id:29,area:4,connections:[0,28,31,32,33],name:"Kamchatka",coords:[876,64]},
               {node_id:30,area:4,connections:[17,26,34,35,36],name:"Afghanistan",coords:[654,134]},
               {node_id:31,area:4,connections:[27,28,29,32],name:"Irkutsk",coords:[778,96]},
               {node_id:32,area:4,connections:[27,29,31,33,34],name:"Mongolia",coords:[789,126]},
               {node_id:33,area:4,connections:[29,32],name:"Japan",coords:[909,158]},
               {node_id:34,area:4,connections:[26,27,30,32,36,37],name:"China",coords:[77,174]},
               {node_id:35,area:4,connections:[17,19,21,22,30,36],name:"Middle East",coords:[597,174]},
               {node_id:36,area:4,connections:[30,34,35,37],name:"India",coords:[765,211]},
               {node_id:37,area:4,connections:[34,36,38],name:"Siam",coords:[797,237]},
               {node_id:38,area:5,connections:[37,39,40],name:"Indonesia",coords:[834,298]},
               {node_id:39,area:5,connections:[38,40,41],name:"New Guinea",coords:[935,319]},
               {node_id:40,area:5,connections:[38,39,41],name:"Western Australia",coords:[852,402]},
               {node_id:41,area:5,connections:[39,40,41],name:"Eastern Australia",coords:[928,394]}
    ],
    areas: [
            {area_id:0,value:5,nodes:[0,1,2,3,4,5,6,7,8]},
            {area_id:1,value:2,nodes:[9,10,11,12]},
            {area_id:2,value:5,nodes:[13,14,15,16,17,18,19]},
            {area_id:3,value:3,nodes:[20,21,22,23,24,25]},
            {area_id:4,value:7,nodes:[26,27,28,29,30,31,32,33,34,35,36,37]},
            {area_id:5,value:2,nodes:[38,39,40,41]}}
    ],
    canvas: null,
    context: null,
    
    initialize: function() {
        this.canvas = $('#riskgui')[0];
        this.context = this.canvas.getContext("2d");
    }

    render: function() {
        for (var node in this.model) {
            node_data = this.nodeData[node.number];
            node_id = nodedata.node_id;
            node_coords = node_data.coords;
            for (neighbor_id in node_data.connections) {
                if neighbor_id > node_id:
                    neighbor_data = this.nodeData[neighbor_id];
                    neighbor_coords = neighbor_data.coords;
                    drawLine(this.context, node_cords[0], neighbor_coords[0], node_coords[1], neighbor_coords[1]);
            }
            this.draw_node(node);
        }
    }
    
    draw_node: function(node) {
        x = nodeCoords[node.number][0];
        y = nodeCoords[node.number][1];
        units = node.units;
        this.context.fillStyle("#000000");
        color = "#999999";
        if (node.player == local_player) {
            color = "#0000FF";
        } else if (game.contains(node.player)) {
            color = "#FF0000";
        }
        this.context.strokeStyle(color);
        drawCircle(this.context, x, y, 12);
        this.context.font = "12pt Arial";
        this.context.fillText(units, x-12, y-12);
    }
    
});

var GameView = Backbone.View.extend({

//TODO:
//Write a view class for the Game (contains all game players) container
//NOTE: The local player is also stored in the variable local_player, and the
//collection localPlayerCollect you may use the LocalPlayerView if you want
//to specify behavior for localplayer specifically
});

var LocalPlayerView = Backbone.View.extend({

//TODO:
//Write a view class for the LocalPlayerCollect (contains the local player) collection
})


var LobbyView = Backbone.View.extend({

//TODO:
//Write a view class for the Lobby (contains all lobby players) container
});

var ActionsView = Backbone.View.extend({

//TODO:
//Write a view class for the actions (contains current action) model.
});

var TurnTimerView = Backbone.View.extend({

//TODO:
//Write a view class for the turn timer (contains player turn and current time WHICH NEEDS IMPLEMENTING) moel
});

var DisconnectedPlayersView = Backbone.View.extend({

//TODO:
//Write a view class for the disconnected users (contains all the players not currently in game) container
});

//Instances of views
var nodeView = new NodeView({ collection : Nodes});
var gameView = new GameView({ collection : Game});
var localPlayerView = new LocalPlayerView({ collection : LocalPlayerCollect});
var lobbyView = new LobbyView ({ collection : Lobby});
var actionsView = new ActionsView ({ model : Actions});
var turnTimerView = new TurnTimerView ({ model : TurnTimer });
var disconnectedPlayersView = new DisconnectedPlayersView ({ collection : DisconnectedPlayers});