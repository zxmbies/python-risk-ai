var MESSAGE_TYPE = "message_type";

var MESSAGE_TYPE_NEW_GAME = "new_game";
var MESSAGE_TYPE_BEGIN_TURN = "begin_turn";
var MESSAGE_TYPE_PLACE_SHIPS = "place_ships";
var MESSAGE_TYPE_DRAW_CARD = "draw_card";
var MESSAGE_TYPE_ERROR = "error";
var MESSAGE_TYPE_PLAYER_CONNECT = "player_connect";
var MESSAGE_TYPE_DRAW_CARD = "draw_card";
var MESSAGE_TYPE_PLAY_CARDS = "play_cards";
var MESSAGE_TYPE_DECLARE_ATTACK = "declare_attack";
var MESSAGE_TYPE_DEFEND_ROLL = "defend_roll";
var MESSAGE_TYPE_RESOLVE_ATTACK = "resolve_attack";
var MESSAGE_TYPE_MOVE_SHIPS = "move_ships";
var MESSAGE_TYPE_PLAYER_RECONNECT = "player_reconnect";
var MESSAGE_TYPE_GAME_WON = "game_won";
var MESSAGE_TYPE_DISCONNECTED_USER = "disconnected_user";

var FIELD_ID_CARDS = "cards";
var FIELD_ID_NUM_CARDS = "num_cards";
var FIELD_ID_MAP = "map";
var FIELD_ID_PLAYERS = "players";
var FIELD_ID_PLAYER_ID = "player_id";
var FIELD_ID_NUM_SHIPS = "num_ships";
var FIELD_ID_NODE_ID = "node_id";
var FIELD_ID_ERROR_TYPE = "error_type";
var FIELD_ID_CARD = "card";
var FIELD_ID_NUM_DICE = "num_dice";
var FIELD_ID_START_NODE = "start_node";
var FIELD_ID_END_NODE = "end_node";
var FIELD_ID_ATTACKER_LOSSES = "attacker_losses";
var FIELD_ID_DEFENDER_LOSSES = "defender_losses";
var FIELD_ID_USERNAME = "username";
var FIELD_ID_OPPONENT = "opponent";
var FIELD_ID_CAPTURED = "captured";

var MESSAGE_END_TURN = "end_turn";
var MESSAGE_END_SETUP = "end_setup";
var MESSAGE_TIMER_START = "timer_start";
var MESSAGE_TIMER_PAUSE = "timer_pause";
var MESSAGE_END_GAME = "end_game";

function placeShips(ships, node) {
    return {
        MESSAGE_TYPE: MESSAGE_TYPE_PLACE_SHIPS,
        FIELD_ID_NUM_SHIPS: ships,
        FIELD_ID_NODE_ID: node
    };
}

function playCards(cards) {
    return {
        MESSAGE_TYPE: MESSAGE_TYPE_PLAY_CARDS,
        FIELD_ID_CARDS: cards
    };
}

function declareAttack(dice, start, end) {
    return {
        MESSAGE_TYPE: MESSAGE_TYPE_DECLARE_ATTACK,
        FIELD_ID_NUM_DICE: dice,
        FIELD_ID_START_NODE: start,
        FIELD_ID_END_NODE: end,
    };
}

function defendRoll(dice) {
    return {
        MESSAGE_TYPE: MESSAGE_TYPE_DEFEND_ROLL,
        FIELD_ID_NUM_DICE: dice
    };
}

function moveShips(ships, start, end) {
    return {
        MESSAGE_TYPE: MESSAGE_TYPE_MOVE_SHIPS,
        FIELD_ID_NUM_SHIPS: ships,
        FIELD_ID_START_NODE: start,
        FIELD_ID_END_NODE: end
    };
}

function playerReconnect(username) {
    return {
        MESSAGE_TYPE: MESSAGE_TYPE_PLAYER_RECONNECT,
        FIELD_ID_USERNAME: username
    };
}

function playerConnect(username, opponent) {
    return {
        MESSAGE_TYPE: MESSAGE_TYPE_PLAYER_CONNECT,
        FIELD_ID_USERNAME: username,
        FIELD-ID_OPPONENT: opponent
    };
}

function handleMessage(message, handler) {
    if (message.message_type == MESSAGE_TYPE_NEW_GAME) {
        handler.newGame(message.map, message.players);
    } else if(message.message_type == MESSAGE_TYPE_BEGIN_TURN) {
        hander.beginTurn(message.player_id, message.players);
    } else if(message.message_type == MESSAGE_TYPE_PLACE_SHIPS) {
        handler.placeShips(message.num_ships, message.node_id);
    } else if(message.message_type == MESSAGE_TYPE_ERROR) {
        handler.error(message.error_type);
    } else if(message.message_type == MESSAGE_TYPE_PLAYER_CONNECT) {
        handler.playerConnect(message.player_id);
    } else if(message.message_type == MESSAGE_TYPE_DRAW_CARD) {
        handler.drawCard(message.card);
    } else if(message.message_type == MESSAGE_TYPE_PLAY_CARDS) {
        handler.playCards(message.num_ships);
    } else if(message.message_type == MESSAGE_TYPE_DECLARE_ATTACK) {
        handler.declareAttack(message.num_dice, message.start_node, message.end_node);
    } else if(message.message_type == MESSAGE_TYPE_RESOLVE_ATTACK) {
        handler.resolveAttack(message.attacker_losses, message.defender_losses, message.captured);
    } else if(message.message_type == MESSAGE_TYPE_MOVE_SHIPS) {
        handler.moveShips(message.num_ships, message.start_node, message.end_node);
    } else if(message.message_type == MESSAGE_TYPE_GAME_WON) {
        handler.gameWon(message.username);
    } else if(message.message_type == MESSAGE_TYPE_PLAYER_RECONNECT) {
        handler.playerReconnect(message.username);
    } else if(message.message_type == MESSAGE_TYPE_DISCONNECTED_USER) {
        handler.disconnectedUser(message.username);
    } 
}