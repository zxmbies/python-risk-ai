var address = "ws://localhost:8080/message";
var ws;
var player_id;
var username = "John";
var nodes = new Array();
var cards = new Array();
var units_to_place = 0;
var n_units_to_place = 0;
var attack_node;
var defend_node;

function ParseNodes() {
    var string = "";
    for (node in nodes) {
        string = string + node + ": Player = " + nodes[node].player
                + ", Units = " + nodes[node].units + "<br>";
    }
    return string;
}
function ParseUnits() {
    return "Player: " + units_to_place + ", Neutral: " + n_units_to_place;
}
function ParseCards() {
    var string = "";
    for (card in cards) {
        string = string + cards[card].node_id + ": Card type = "
                + cards[card].card_type + "<br>";
    }
    return string;
}
function RemoveCards(num1, num2, num3) {
    array_temp = new Array();
    for (card in cards) {
        if (cards[card].node_id != num1 && cards[card].node_id != num2
                && cards[card].node_id != num3) {
            array_temp.push(cards[card]);
        }
        return array_temp;
    }
}
function Connect() {
    var msg = {
        "message_type" : "player_connect",
        "username" : "John",
        "opponent" : "Ringo"
    };
    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser!");
        // Let us open a web socket
        ws = new WebSocket(address);
        ws.onopen = function() {
            // Web Socket is connected, send data using send()
            ws.send(JSON.stringify(msg));
            console.log("Message is sent...");
        };
        ws.onmessage = function(evt) {
            var received_msg = JSON.parse(evt.data);
            console.log("Message is received...");
            switch (received_msg.message_type) {
            case "new_game":
                for (person in (received_msg.players)) {
                    if (received_msg.players[person].player_name == username) {
                        player_id = received_msg.players[person].player_id;
                    }
                }
                for (node in received_msg.map.nodes) {
                    nodes[node] = {
                        player : received_msg.map.nodes[node].player.player_id,
                        units : received_msg.map.nodes[node].num_ships
                    };
                }
                document.getElementById("player_number").innerHTML = player_id;
                document.getElementById("nodes").innerHTML = ParseNodes();
                break;
            case "begin_turn":
                if (received_msg.player_id == player_id) {
                    units_to_place = received_msg.players[player_id].reserved_ships;
                    n_units_to_place = received_msg.players[0].reserved_ships;
                }
                console.log(received_msg.players[player_id].setup_turns)
                document.getElementById("ships").innerHTML = ParseUnits();
                ws.send(JSON.stringify(start_timer));
                break;
            case "place_ships":
                console.log("Node: " + received_msg.node_id + ", num_ships:"
                        + received_msg.num_ships)
                nodes[received_msg.node_id].units += received_msg.num_ships;
                if (nodes[received_msg.node_id].player == player_id) {
                    units_to_place = units_to_place - received_msg.num_ships;
                } else if (nodes[received_msg.node_id].player == 0) {
                    n_units_to_place = n_units_to_place
                            - received_msg.num_ships;
                }
                document.getElementById("nodes").innerHTML = ParseNodes();
                document.getElementById("ships").innerHTML = ParseUnits();
                break;
            case "declare_attack":
                attack_node = received_msg.start_node;
                defend_node = received_msg.end_node;
                break;
            case "resolve_attack":
                nodes[attack_node].units = nodes[attack_node].units
                        - received_msg.attacker_losses;
                nodes[defend_node].units = nodes[defend_node].units
                        - received_msg.defender_losses;
                if (received_msg.captured) {
                    nodes[defend_node].player = nodes[attack_node].player
                }
                document.getElementById("nodes").innerHTML = ParseNodes();
                break;
            case "move_ships":
                nodes[received_msg.start_node].units = nodes[received_msg.start_node].units
                        - received_msg.num_ships;
                nodes[received_msg.end_node].units = nodes[received_msg.end_node].units
                        + received_msg.num_ships;
                document.getElementById("nodes").innerHTML = ParseNodes();
                break;
            case "draw_card":
                cards.push(received_msg.card);
                document.getElementById("cards").innerHTML = ParseCards();
                break;
            case "play_cards":
                units_to_place = units_to_place + received_msg.num_ships
                document.getElementById("ships").innerHTML = ParseUnits();
                break;
            }
        };
        ws.onclose = function() {
            // websocket is closed.
            console.log("Connection is closed...");
        };
    } else {
        // The browser doesn't support WebSocket
        alert("WebSocket NOT supported by your Browser!");
    }
}

function Reconnect() {
    var msg = {
        "message_type" : "player_reconnect",
        "username" : "John"
    };
    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser!");
        // Let us open a web socket
        ws = new WebSocket(address);
        
        ws.onopen = function() {
            // Web Socket is connected, send data using send()
            ws.send(JSON.stringify(msg));
            console.log("Message is sent...");
        };
        ws.onmessage = function(evt) {
            var received_msg = JSON.parse(evt.data);
            console.log("Message is received...");
            switch (received_msg.message_type) {
            case "new_game":
                for (person in (received_msg.players)) {
                    if (received_msg.players[person].player_name == username) {
                        player_id = received_msg.players[person].player_id;
                    }
                }
                for (node in received_msg.map.nodes) {
                    nodes[node] = {
                        player : received_msg.map.nodes[node].player.player_id,
                        units : received_msg.map.nodes[node].num_ships
                    };
                }
                document.getElementById("player_number").innerHTML = player_id;
                document.getElementById("nodes").innerHTML = ParseNodes();
                break;
            case "begin_turn":
                if (received_msg.player_id == player_id) {
                    units_to_place = received_msg.players[player_id].reserved_ships;
                    n_units_to_place = received_msg.players[0].reserved_ships;
                }
                document.getElementById("ships").innerHTML = ParseUnits();
                ws.send(JSON.stringify(start_timer));
                break;
            case "place_ships":
                // console.log("Node: " + received_msg.node_id + ", num_ships:"
                // + received_msg.num_ships)
                nodes[received_msg.node_id].units += received_msg.num_ships;
                if (nodes[received_msg.node_id].player == player_id) {
                    units_to_place = units_to_place - received_msg.num_ships;
                } else if (nodes[received_msg.node_id].player == 0) {
                    n_units_to_place = n_units_to_place
                            - received_msg.num_ships;
                }
                document.getElementById("nodes").innerHTML = ParseNodes();
                document.getElementById("ships").innerHTML = ParseUnits();
                break;
            case "declare_attack":
                attack_node = received_msg.start_node;
                defend_node = received_msg.end_node;
                break;
            case "resolve_attack":
                nodes[attack_node].units = nodes[attack_node].units
                        - received_msg.attacker_losses;
                nodes[defend_node].units = nodes[defend_node].units
                        - received_msg.defender_losses;
                if (received_msg.captured) {
                    nodes[defend_node].player = nodes[attack_node].player
                }
                document.getElementById("nodes").innerHTML = ParseNodes();
                break;
            case "move_ships":
                nodes[received_msg.start_node].units = nodes[received_msg.start_node].units
                        - received_msg.num_ships;
                nodes[received_msg.end_node].units = nodes[received_msg.end_node].units
                        + received_msg.num_ships;
                document.getElementById("nodes").innerHTML = ParseNodes();
                break;
            case "draw_card":
                cards.push(received_msg.card);
                document.getElementById("cards").innerHTML = ParseCards();
                break;
            }
        };
        ws.onclose = function() {
            // websocket is closed.
            console.log("Conection  is closed...");
        };
    } else {
        // The browser doesn't support WebSocket
        alert("WebSocket NOT supported by your Browser!");
    }
}

function Place() {
    var node_id = parseInt(document.getElementById("area").value);
    var num_units = parseInt(document.getElementById("units").value);
    var place_msg = {
        "message_type" : "place_ships",
        "num_ships" : num_units,
        "node_id" : node_id
    };
    ws.send(JSON.stringify(place_msg));
}
function Attack() {
    var node_from = parseInt(document.getElementById("attack_from").value);
    var node_to = parseInt(document.getElementById("attack_to").value);
    var num_dice = parseInt(document.getElementById("num_dice").value);
    var attack_msg = {
        "message_type" : "declare_attack",
        "num_dice" : num_dice,
        "start_node" : node_from,
        "end_node" : node_to
    };
    
    ws.send(JSON.stringify(attack_msg));
}
function Move() {
    var start_node = parseInt(document.getElementById("start_node").value);
    var end_node = parseInt(document.getElementById("end_node").value);
    var num_ships = parseInt(document.getElementById("num_ships").value);
    var move_msg = {
        "message_type" : "move_ships",
        "num_ships" : num_ships,
        "start_node" : start_node,
        "end_node" : end_node
    };
    ws.send(JSON.stringify(move_msg));
}
function EndTurn() {
    var end_msg = {
        "message_type" : "end_turn"
    };
    ws.send(JSON.stringify(end_msg));
}
function PlayCards() {
    var card1 = parseInt(document.getElementById("card1").value);
    var card2 = parseInt(document.getElementById("card2").value);
    var card3 = parseInt(document.getElementById("card3").value);
    for (card in cards) {
        if (card1 == cards[card].node_id) {
            card1 = cards[card];
        }
        if (card2 == cards[card].node_id) {
            card2 = cards[card];
        }
        if (card3 == cards[card].node_id) {
            card3 = cards[card];
        }
    }
    card1_s = JSON.stringify(card1);
    card2_s = JSON.stringify(card2);
    card3_s = JSON.stringify(card3);
    
    var cards_ns = {
        "card1" : card1_s,
        "card2" : card2_s,
        "card3" : card3_s
    };
    var cards_s = JSON.stringify(cards_ns);
    
    var cards_msg = {
        "message_type" : "play_cards",
        "cards" : cards_s
    };
    ws.send(JSON.stringify(cards_msg));
    cards = RemoveCards(card1.node_id, card2.node_id, card3.node_id);
    document.getElementById("cards").innerHTML = ParseCards();
}
function Defense() {
    var defense_dice = parseInt(document.getElementById("defense_dice").value);
    var defense_msg = {
        "message_type" : "defend_roll",
        "num_dice" : defense_dice
    };
    ws.send(JSON.stringify(defense_msg));
}