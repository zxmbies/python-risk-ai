'''
Created on Nov 1, 2013

@author: thedoctor
'''
import threading

class My_Queue:

    def __init__(self):
        self.queue = []
        self.queue_overhead = []
        self.lock = threading.Lock()

    def enqueue(self):
        self.lock.acquire()
        start_lock = threading.Lock()
        start_lock.acquire()
        if self.queue:
            lock = self.queue[-1]
            self.queue.append(start_lock)
            self.queue_overhead.append([lock, start_lock])
            self.lock.release()
            lock.acquire()  
        else:
            self.queue.append(start_lock)
            self.queue_overhead.append([start_lock])
            self.lock.release()
        print("Enqueue complete, current queue:", self.queue_overhead)
        
    def dequeue(self):
        self.lock.acquire()
        self.queue.pop(0)
        end_lock = self.queue_overhead.pop(0)
        for lock in end_lock:
            lock.release()
        self.lock.release()
        print("Dequeue complete, current queue:", self.queue_overhead)