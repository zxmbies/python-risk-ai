import pymysql
conn = pymysql.connect(host='localhost', user='risk', passwd='galaxyvincent14', db='galcon')
cur = conn.cursor()
try:
    cur.execute("CREATE table users (id INT NOT NULL AUTO_INCREMENT, username VARCHAR(20) NOT NULL, email VARCHAR(50) NOT NULL, registration_date DATE NOT NULL, salt BLOB(512), hash BLOB(512), wins INT NOT NULL, losses INT NOT NULL, PRIMARY KEY (id));")
except pymysql.err.InternalError:
    print("Internal Error was raised trying to make users table.")
try:
    cur.execute("CREATE table games (id INT NOT NULL AUTO_INCREMENT, player_one INT NOT NULL, player_two INT NOT NULL, winner INT, start_time DATETIME, end_time DATETIME, PRIMARY KEY (id));")
except pymysql.err.InternalError:
    print("Internal Error was raised trying to make games table.")
try:
    cur.execute("CREATE table cookies (id BIGINT NOT NULL AUTO_INCREMENT, player_id INT NOT NULL, hash VARCHAR(512) NOT NULL, salt VARCHAR(512) NOT NULL, PRIMARY KEY (id));")
except pymysql.err.InternalError:
    print("Internal Error was raised trying to make cookies table.")
try:
    cur.execute("CREATE table unconf_users (id INT NOT NULL AUTO_INCREMENT, player_id INT NOT NULL, code VARCHAR(128) NOT NULL, PRIMARY KEY (id));")
except pymysql.err.InternalError:
    print("Internal Error was raised trying to make unconf_users table.")
cur.close()
conn.close()
