'''
@author: kristoffe
'''
from tornado import web,template


class GameHandler(web.RequestHandler):
    
        
    def get(self):
        loader = template.Loader("templates")
        self.write(loader.load("game.html").generate())