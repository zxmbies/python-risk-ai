import tornado.web
import pymysql
import re
import utilities.escape_bytes
from pbkdf2.pbkdf2 import PBKDF2

ACCOUNT = "http://lenin.duckdns.org:8080/account"
GAME = "http://lenin.duckdns.org/risk/client/index.html"

class Login(tornado.web.RequestHandler):
    
    def get(self):
        if self.get_secure_cookie("login"):
            self.write('''
                <script type = "text/javascript">
                window.location = "'''+GAME+'''"
                </script>
                ''')
            return
        self.write('''
            <form action = "/login" method = "post">
            Username: <input type = "text" name="username"><br>
            Password: <input type = "password" name ="password"><br>
            Stay Logged In? <input type= "checkbox" name="stay_logged_in"><br>
            <input type = "submit" value = "Login">
            </form>
            <a href="'''+ACCOUNT+'''">Create Account</a>
            '''
        )
        return
        
    def post(self):
        username = self.get_argument('username', '')
        password = self.get_argument('password', '')
        stay_logged_in = self.get_argument('stay_logged_in', '')
        
        print (username)
        print (password)
        print (stay_logged_in)
        
        username_re = "^[a-zA-Z0-9_-]{3,16}$"
        password_re = "^[a-zA-Z0-9_-]{6,18}$"
        
        if not re.match(username_re, username) or not re.match(password_re, password):
            self.write('''
            <form action = "/login" method = "post">
            Username: <input type = "text" name="username"><br>
            Password: <input type = "password" name ="password"><br>
            Stay Logged In? <input type= "checkbox" name="stay_logged_in"><br>
            <input type = "submit" value = "Login">
            </form>
            <p style = "color: red;">Invalid Username or Password</p>
            <a href="'''+ACCOUNT+'''">Create Account</a>
            '''
            )
            return
    
        conn = pymysql.connect(host='localhost', user='risk', passwd='galaxyvincent14', db='galcon')
        cur = conn.cursor()
        
        cur.execute('SELECT salt, hash FROM users WHERE username ="'+username+'";')
        
        salt = None
        
        for row in cur:
            salt = row[0]
            hash_from_db = str(row[1])
            
        if not salt:
            self.write('''
            <form action = "/login" method = "post">
            Username: <input type = "text" name="username"><br>
            Password: <input type = "password" name ="password"><br>
            Stay Logged In? <input type= "checkbox" name="stay_logged_in"><br>
            <input type = "submit" value = "Login">
            </form>
            <p style = "color: red;">Invalid Username or Password</p>
            <a href="'''+ACCOUNT+'''">Create Account</a>
            '''
            )
            cur.close()
            conn.close()
            return
        
        print (salt)
        myHash = str('b"'+utilities.escape_bytes.escape_bytes(PBKDF2(password, salt).read(64))+'"')
        print("Generated Hash:", myHash)
        print("Database Hash:", hash_from_db)
        
        if hash_from_db == myHash:
            if stay_logged_in:
                self.set_secure_cookie("login", username, expires_days=31)
            else:
                self.set_secure_cookie("login", username, expires_days=None)
            self.write('''
                <script type = "text/javascript">
                window.location = "'''+GAME+'''"
                </script>
                ''')
        cur.close()
        conn.close()
