'''
Created on Jul 16, 2013

@author: kristoffe
'''

import tornado
import os.path

from web.game import GameHandler
from tornado.web import Application

def main():
    app = Application([
        (r"/game/", GameHandler)
    ], debug=True, static_path=os.path.join(os.getcwd(), 'content'))
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    main()