import tornado.web
import re
import os
import utilities.escape_bytes
from pbkdf2.pbkdf2 import PBKDF2
import pymysql
import datetime

LOGIN = "http://lenin.duckdns.org:8080/login"

class Account(tornado.web.RequestHandler):
    
    def get(self):
        self.write('''
            <form action = "/account" method = "post">
            Username: <input type = "text" name="username"><br>
            Password: <input type = "password" name ="password"><br>
            Re-enter Password: <input type = "password" name = "re_password"><br>
            Email: <input type= "text" name="email"><br>
            <input type = "submit" value = "Create Account">
            </form>
            <a href="'''+LOGIN+'''">Back to Login</a>
            '''
        )
        return
    
    def post(self):
        username = self.get_argument('username', '')
        password = self.get_argument('password', '')
        re_password = self.get_argument('re_password', '')
        email = self.get_argument('email', '')
        
        if password != re_password:
            self.write('''
                <form action = "/account" method = "post">
                Username: <input type = "text" name="username"><br>
                Password: <input type = "password" name ="password"><br>
                Re-enter Password: <input type = "password" name = "re_password"><br>
                Email: <input type= "text" name="email"><br>
                <input type = "submit" value = "Create Account">
                </form>
                <p style = "color: red;">Passwords do not match</p>
                <a href="'''+LOGIN+'''">Back to Login</a>
                '''
            )
            return
        
        username_re = "^[a-zA-Z0-9_-]{3,16}$"
        password_re = "^[a-zA-Z0-9_-]{6,18}$"
        email_re = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
        
        
        if not re.match(username_re, username) or not re.match(password_re, password) or not re.match(email_re, email):
            self.write('''
                <form action = "/account" method = "post">
                Username: <input type = "text" name="username"><br>
                Password: <input type = "password" name ="password"><br>
                Re-enter Password: <input type = "password" name = "re_password"><br>
                Email: <input type= "text" name="email"><br>
                <input type = "submit" value = "Create Account">
                </form>
                <p style = "color: red;">Invalid Characters</p>
                <a href="'''+LOGIN+''''">Back to Login</a>
                '''
            )
            return

        conn = pymysql.connect(host='localhost', user='risk', passwd='galaxyvincent14', db='galcon')
        cur = conn.cursor()
        
        cur.execute('SELECT * FROM users WHERE username = "'+username+'";')
        
        salt = utilities.escape_bytes.escape_bytes(os.urandom(64))
        myHash = PBKDF2(password, salt).read(64)
        myHash = utilities.escape_bytes.escape_bytes(myHash)
        insert_string = ("INSERT INTO users (username, email, registration_date, salt, hash, wins, losses) " +
                    'VALUES ("'+username+'", "'+email+'", "'+datetime.date.today().strftime("%Y-%m-%d")+'","'+salt+'", "'+myHash+'", 0, 0);')
        print(insert_string)
        cur.execute(insert_string)
        
        conn.commit()
        cur.close()
        conn.close()
        
        self.write('''
            <script type = "text/javascript">
            window.location = "'''+LOGIN+'''"
            </script>
            '''
            )
        return
