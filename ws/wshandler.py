import tornado.websocket
import protocol.gameprotocol
import model.modelHandler
import time
import copy
import controller.controller_module
import pymysql

class WSHandler(tornado.websocket.WebSocketHandler):

    def open(self):
        if self.get_secure_cookie("login"):
            MH = protocol.msghandler.HandlerImplementation(self)
            logicFunctions = MH.lobby_connect(self.get_secure_cookie("login"))
            if logicFunctions:
                controller.controller_module.processFunctions(logicFunctions)
            self.write_message(protocol.gameprotocol.send_username(self.get_secure_cookie("login").decode("utf-8")))
            print("Something is happening")
    def on_message(self, message):
        controller.controller_module.WebSocketControl(self, message)

    def on_close(self):
        if controller.controller_module.socket_to_game.iterate(self):
            game = controller.controller_module.socket_to_game.get(self)
            model.modelHandler.setter.acquire_lock(game)
            if controller.controller_module.socket_to_game.iterate(self):
                player = model.modelHandler.getter.player_by_socket(game, self)
                model.modelHandler.setter.del_game_socket(game, self)
                model.modelHandler.setter.del_socket_to_username(game, self)
                controller.controller_module.socket_to_game.remove(self)
                model.modelHandler.setter.disconnected_player(game, player, time.time())
                for socket in model.modelHandler.getter.game_sockets(game):
                    socket.write_message(protocol.gameprotocol.disconnected_user(model.modelHandler.getter.player_id(player)))
            model.modelHandler.setter.release_lock(game)
        lobby = controller.controller_module.lobby
        if model.modelHandler.getter.socket_in_lobby(lobby, self):
            model.modelHandler.setter.acquire_lobby_lock(lobby)
            if model.modelHandler.getter.socket_in_lobby(lobby, self):
                username = model.modelHandler.getter.lobby_username_by_socket(lobby, self)
            else: return
            model.modelHandler.setter.del_lobby_user(username, lobby)
            model.modelHandler.setter.remove_challenges_from_lobby(username, "", lobby)
            for queue_entry in copy.copy(model.modelHandler.getter.lobby_queue(lobby)):
                if queue_entry[0] == username:
                    model.modelHandler.setter.del_lobby_queue_entry(lobby, queue_entry)
            for user in model.modelHandler.getter.lobby_users(lobby):
                print("Does this happen first?")
                model.modelHandler.getter.lobby_users(lobby)[user].write_message(protocol.gameprotocol.lobby_disconnect(username))    
            model.modelHandler.setter.release_lobby_lock(lobby)
