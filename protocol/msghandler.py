import protocol.gameprotocol
import copy
import functools
import utilities.cm
import utilities.gm
import threading
import model.modelHandler
import controller.controller_module

class HandlerImplementation(protocol.gameprotocol.MessageHandler):

    def __init__(self, socket):
        self.socket = socket


    def place_ships(self, num_ships, node_id):
        functions = []
        '''The player has selected a node with id "node_id" to place "num_ships" ships'''
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return []
        
        '''overhead functions, repeated often but f.i.'''
        game = controller.controller_module.socket_to_game.get(self.socket)
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
        neutral = model.modelHandler.getter.player_by_number(game, 0)
        
        '''parsing'''
        if not isinstance(num_ships, int) or not isinstance(node_id, int) or node_id < 0 or node_id >= len(game.game_map.nodes):
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            return functions
        
        '''bulk of the function'''
        node = model.modelHandler.getter.node_by_node_id(game, node_id)
        if model.modelHandler.getter.player_id(player) == model.modelHandler.getter.player_turn(game):
            if (model.modelHandler.getter.player_by_node(node) == player 
                and model.modelHandler.getter.reserved_ships(player) >= num_ships 
                and model.modelHandler.getter.num_cards(player) < 6):
                functions.append(functools.partial(controller.controller_module.place_ships, game, node, player, num_ships))
            elif (model.modelHandler.getter.player_by_node(node) == neutral 
                  and model.modelHandler.getter.reserved_ships(neutral) >= num_ships 
                  and model.modelHandler.getter.reserved_ships(player) == 0):
                functions.append(functools.partial(controller.controller_module.place_ships, game, node, neutral, num_ships))
                if model.modelHandler.getter.reserved_ships(neutral) == 0:
                    functions.append(functools.partial(controller.controller_module.end_turn, player))
        functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
        functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
        return functions
            
    def end_turn(self):
        '''The player has selected to end their turn.'''
        functions = []
        '''overhead functions, repeated often but f.i.'''
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return []
        
        game = controller.controller_module.socket_to_game.get(self.socket)
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
        neutral = model.modelHandler.getter.player_by_number(game, 0)
        
        '''bulk of the function'''
        if model.modelHandler.getter.player_id(player) == model.modelHandler.getter.player_turn(game):
            
            '''this should probably be relocated, but where?'''
            if model.modelHandler.getter.num_cards(player) == 6 and not model.modelHandler.getter.captured_country(player):
                card1, card2, card3 = utilities.gm.get_match(model.modelHandler.getter.cards(player))
                functions.append(functools.partial(controller.controller_module.play_cards, game, player, card1, card2, card3, self.socket))
            functions.append(functools.partial(controller.controller_module.resolve_capture, game))
            functions.append(functools.partial(controller.controller_module.auto_place_ships, game, player, model.modelHandler.getter.game_map(game)))
            functions.append(functools.partial(controller.controller_module.auto_place_ships, game, neutral, model.modelHandler.getter.game_map(game)))
            functions.append(functools.partial(controller.controller_module.end_turn, player, game))
        functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
        functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
        return functions

    def play_cards(self, cards):
        functions = []
        '''This function is pretty dirty, but I can't think of any ideas right now.'''
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return []
        
        game = controller.controller_module.socket_to_game.get(self.socket)
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
        
        '''parsing'''
        if not isinstance(cards, (str, tuple, dict, list)) or len(cards) != 3:
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            return functions
        for card in cards:
            if not hasattr(card, "card_type") or not hasattr(card, "node_id"):
                functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
                functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
                return functions
        
        (card1, card2, card3) = (cards[0], cards[1], cards[2])
        check_has_cards = 0
        for card in model.modelHandler.getter.cards(player):
            if (model.modelHandler.getter.card_type(card) == model.modelHandler.getter.card_type(card1) 
                and model.modelHandler.getter.card_node_id(card) == model.modelHandler.getter.card_node_id(card1)):
                check_has_cards += 1
                card_found1 = card
            if (model.modelHandler.getter.card_type(card) == model.modelHandler.getter.card_type(card2) 
                and model.modelHandler.getter.card_node_id(card) == model.modelHandler.getter.card_node_id(card2)):
                check_has_cards += 1
                card_found2 = card
            if (model.modelHandler.getter.card_type(card) == model.modelHandler.getter.card_type(card3) 
                and model.modelHandler.getter.card_node_id(card) == model.modelHandler.getter.card_node_id(card3)):
                check_has_cards += 1
                card_found3 = card
        if (check_has_cards == 3 and model.modelHandler.getter.can_play_cards(player) == True and utilities.cm.check_match(card_found1, card_found2, card_found3) == True):
            functions.append(functools.partial(controller.controller_module.play_cards, game, player, card_found1, card_found2, card_found3, self.socket))
        functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
        functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
        return functions
    
    def declare_attack(self, num_dice, start_node, end_node):
        '''The player has declared an attack'''
        functions = []
        '''These five lines: same shit, different function'''
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return []
        
        game = controller.controller_module.socket_to_game.get(self.socket)
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
        neutral = model.modelHandler.getter.player_by_number(game, 0)        
        
        '''parsing'''
        if (not isinstance(num_dice, int) or not isinstance(start_node, int) or not isinstance(end_node, int) or start_node < 0 or end_node < 0 
			  or start_node >= model.modelHandler.getter.total_num_nodes(model.modelHandler.getter.game_map(game)) 
              or end_node >= model.modelHandler.getter.total_num_nodes(model.modelHandler.getter.game_map(game))):
            print ("Num dice is int",  isinstance(num_dice, int))
            print ("Start node is int", isinstance(start_node, int))
            print ("End node is int", isinstance(end_node, int))
            print ("Start node >= 0", start_node >= 0)
            print ("End node >0", end_node >= 0)
            print ("Start node less than length of nodes", start_node < model.modelHandler.getter.total_num_nodes(model.modelHandler.getter.game_map(game)))
            print ("End node less than length of nodes", end_node < model.modelHandler.getter.total_num_nodes(model.modelHandler.getter.game_map(game)))
            
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            print("Something went wrong in parsing")
            return functions
        
        '''actual function'''
        (start_node, end_node) = (model.modelHandler.getter.node_by_node_id(game, start_node), model.modelHandler.getter.node_by_node_id(game, end_node))
        if (model.modelHandler.getter.player_id(player) == model.modelHandler.getter.player_turn(game) 
            and model.modelHandler.getter.reserved_ships(player) == 0 and model.modelHandler.getter.declared_attack(game)["CAPTURED"] == False):
            print("First check passed")
            print("Player is correct?", model.modelHandler.getter.player_by_node(start_node) == player)
            print("End player is Neutral?", model.modelHandler.getter.player_by_node(end_node) == neutral)
            print("Num ships is correct?", model.modelHandler.getter.num_ships(start_node) > num_dice)
            print("Connection exists", model.modelHandler.getter.node_id(end_node) in model.modelHandler.getter.connections(start_node))
            
            print ("Neutral is", neutral.player_id)
            print("Neutral node is", model.modelHandler.getter.player_by_node(end_node).player_id)
            if (model.modelHandler.getter.player_by_node(start_node) == player and not model.modelHandler.getter.player_by_node(end_node) == player 
                and not model.modelHandler.getter.player_by_node(end_node) == neutral and model.modelHandler.getter.num_ships(start_node) > num_dice 
                and model.modelHandler.getter.node_id(end_node) in model.modelHandler.getter.connections(start_node)):
                functions.append(functools.partial(controller.controller_module.declare_attack, game, player, num_dice, start_node, end_node, False))
            elif (model.modelHandler.getter.player_by_node(start_node) == player and model.modelHandler.getter.player_by_node(end_node) == neutral 
                  and model.modelHandler.getter.num_ships(start_node) > num_dice and
                    model.modelHandler.getter.node_id(end_node) in model.modelHandler.getter.connections(start_node)):
                print("Appending appropriate function call")
                functions.append(functools.partial(controller.controller_module.declare_attack, game, player, num_dice, start_node, end_node, True))
        print("Ending processing")
        functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
        functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
        
        return functions
    
    def defend_roll(self, num_dice):
        functions = []
        
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return []
        game = controller.controller_module.socket_to_game.get(self.socket)
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
        
        '''parsing'''
        if not isinstance(num_dice, int):
            print("failed to parse defense roll")
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            return functions
        
        declared_attack = model.modelHandler.getter.declared_attack(game)
        if (declared_attack["END_NODE"] is not None and model.modelHandler.getter.player_by_node(declared_attack["END_NODE"]) is player and
            declared_attack["CAPTURED"] is False and num_dice <= model.modelHandler.getter.num_ships(declared_attack["END_NODE"])):
            functions.append(functools.partial(controller.controller_module.resolve_attack, game, player, num_dice))
        functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
        functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
        return functions
    
    def move_ships(self, num_ships, start_node, end_node):
        functions = []
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return []
        print("Attempting to move ships")
        game = controller.controller_module.socket_to_game.get(self.socket)
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
        
        '''parsing'''
        if (not isinstance(num_ships, int) or not isinstance(start_node, int) or not isinstance(end_node, int) or start_node < 0 or end_node < 0 
			 or start_node >= model.modelHandler.getter.total_num_nodes(model.modelHandler.getter.game_map(game)) 
              or end_node >= model.modelHandler.getter.total_num_nodes(model.modelHandler.getter.game_map(game))):
            print("Move ships did not parse")
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            return functions
        
        '''function! it's largely if statements, don't be discouraged.'''
        start_node, end_node = model.modelHandler.getter.node_by_node_id(game, start_node), model.modelHandler.getter.node_by_node_id(game, end_node) 
        
        declared_attack = model.modelHandler.getter.declared_attack(game)
        print("Start node owned by player", model.modelHandler.getter.player_by_node(start_node) == player)
        print("End node owned by player", model.modelHandler.getter.player_by_node(end_node) == player)
        print("Ships moved less than ships in node", num_ships < model.modelHandler.getter.num_ships(start_node))
        print("No captured node", declared_attack["CAPTURED"] != True)
        print("Is player turn", model.modelHandler.getter.player_id(player) == model.modelHandler.getter.player_turn(game))
        print("Num ships being moved", num_ships)
        print("Num ships in node", model.modelHandler.getter.num_ships(start_node))
        
        
        if (declared_attack["CAPTURED"] == True and declared_attack["START_NODE"] == start_node and
            declared_attack["END_NODE"] == end_node and declared_attack["ATTACK_DICE"] <= num_ships and
            num_ships < model.modelHandler.getter.num_ships(start_node) 
            and model.modelHandler.getter.player_id(player) == model.modelHandler.getter.player_turn(game)):
            print("Moving ships working")
            functions.append(functools.partial(controller.controller_module.move_ships, game, start_node, end_node, num_ships, True))
            
        elif (model.modelHandler.getter.player_by_node(start_node) == player and
              model.modelHandler.getter.player_by_node(end_node) == player and
              num_ships < model.modelHandler.getter.num_ships(start_node) and declared_attack["CAPTURED"] != True and
              model.modelHandler.getter.player_id(player) == model.modelHandler.getter.player_turn(game)):
            print("Moving ships at end of turn working")
            functions.append(functools.partial(controller.controller_module.move_ships, game, start_node, end_node, num_ships, False))

        print("Exiting fine")
        functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
        functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
        return functions
    
    def start_timer(self):
        functions = []
        
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return []
        '''overhead'''
        print("Preparing to start timer")
        if not controller.controller_module.socket_to_game.iterate(self.socket):
            return
        game = controller.controller_module.socket_to_game.get(self.socket)
        print("Acquired socket to game")
        model.modelHandler.setter.enqueu(game)
        print("Acquired queue")
        model.modelHandler.setter.acquire_lock(game)
        print("Acquired lock")
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
   
        '''function'''
        if (model.modelHandler.getter.player_turn(game) == model.modelHandler.getter.player_id(player)
            and model.modelHandler.getter.turn_timer(game) == False):
            controller.controller_module.start_turn_timer(game, player)
        model.modelHandler.setter.release_lock(game)
        model.modelHandler.setter.dequeue(game)
        return functions
            
    def player_reconnect(self, username):
        
        functions = []
        '''deals with players attempting to reconnect to game'''
        
        '''NOTE: these three lines of code rely on the game existing when the user attempt to connect
        if the game does not exist, the function iterate_and_get returns false, and the game returns
        perhaps it would be better as an inline function, do those exist in python? EDIT: What the fuck.
        Why was I talking about inline functions?'''
        game = controller.controller_module.player_to_game.iterate_and_get(username)
        if game == False:
            return functions
        
        '''rest of overhead'''
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        
        player = model.modelHandler.getter.player_by_username(game, username)
        
        '''feels clean mon.'''
        if player in model.modelHandler.getter.disconnected_players(game):
            functions.append(functools.partial(controller.controller_module.player_reconnect, game, player, self.socket))
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            functions.append(functools.partial(self.start_timer))
        else:
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            
        return functions
    
    def player_connect(self, username, opponent):
        functions = []
        
        if controller.controller_module.player_to_game.iterate(username):
            game = controller.controller_module.player_to_game.get(username)
            model.modelHandler.setter.enqueu(game)
            model.modelHandler.setter.acquire_lock(game)
            if not controller.controller_module.player_to_game.iterate(username) or model.modelHandler.getter.game_started(game):
                functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
                functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
                return functions
            functions.append(functools.partial(controller.controller_module.connect_player, game, username, self.socket))
            functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
            functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            return functions
        else:
            new_game = model.modelHandler.setter.build_world(username)
            model.modelHandler.setter.socket_to_username(new_game, self.socket, username)
            model.modelHandler.setter.game_socket(new_game, self.socket)
            controller.controller_module.player_to_game.set(username, new_game)
            controller.controller_module.player_to_game.set(opponent, new_game)
            controller.controller_module.socket_to_game.set(self.socket, new_game)
            functions.append(functools.partial(threading.Thread(target=new_game.game_loop).start))
            return functions
            
    def lobby_connect(self, username):
        print("player connected!")
        functions = []
        lobby = controller.controller_module.lobby
        username = username.decode("utf-8")
        if controller.controller_module.player_to_game.iterate(username):
            functions.append(functools.partial(self.player_reconnect, username))
            return functions
        
        model.modelHandler.setter.acquire_lobby_lock(lobby)
        if username in model.modelHandler.getter.lobby_users(lobby):
            model.modelHandler.getter.lobby_user(username, lobby).close()
            model.modelHandler.setter.del_lobby_user(username, lobby)
            model.modelHandler.setter.remove_challenges_from_lobby(username, "", lobby)
            for queue_entry in copy.copy(model.modelHandler.getter.lobby_queue(lobby)):
                if queue_entry[0] == username:
                    model.modelHandler.setter.del_lobby_queue_entry(lobby, queue_entry)
            for user in model.modelHandler.getter.lobby_users(lobby):
                model.modelHandler.getter.lobby_users(lobby)[user].write_message(protocol.gameprotocol.lobby_disconnect(username))    

        self.socket.write_message(protocol.gameprotocol.create_lobby(model.modelHandler.getter.lobby_users(lobby)))
        for user in model.modelHandler.getter.lobby_users(lobby):
            model.modelHandler.getter.lobby_user(user, lobby).write_message(protocol.gameprotocol.lobby_connect(username))
        model.modelHandler.setter.lobby_user(username, self.socket, lobby)
        functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby))
        print(functions)
        return functions
        
    def forfeit(self):
        functions = []
        
        game = controller.controller_module.socket_to_game.get(self.socket)
        model.modelHandler.setter.enqueu(game)
        model.modelHandler.setter.acquire_lock(game)
        player = model.modelHandler.getter.player_by_socket(game, self.socket)
        
        functions.append(functools.partial(controller.controller_module.forfeit, game, player))
        functions.append(functools.partial(model.modelHandler.setter.release_lock, game))
        functions.append(functools.partial(model.modelHandler.setter.dequeue, game))
            
        return functions
        
    def join_queue(self):
        functions = []
        lobby = controller.controller_module.lobby
        model.modelHandler.setter.acquire_lobby_lock(lobby)
        if model.modelHandler.getter.socket_in_lobby(lobby, self.socket):
            username = model.modelHandler.getter.lobby_username_by_socket(lobby, self.socket)
        else:
            functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby)) 
            return functions
        
        for queue_entry in model.modelHandler.getter.lobby_queue(lobby):
            if queue_entry[0] == username:
                functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby)) 
                return functions 

        model.modelHandler.setter.join_lobby_queue(username,self,lobby)
        self.socket.write_message(protocol.gameprotocol.join_queue())
        functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby))
        return functions
        
    def challenge_user(self, opponent):
        functions = []
        lobby = controller.controller_module.lobby
        model.modelHandler.setter.acquire_lobby_lock(lobby)
        if model.modelHandler.getter.socket_in_lobby(lobby, self.socket):
            username = model.modelHandler.getter.lobby_username_by_socket(lobby, self.socket)
        else:
            functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby)) 
            print("returning functions")
            return functions
        
        print ("Lobby users:", model.modelHandler.getter.lobby_users(lobby))
        if opponent in model.modelHandler.getter.lobby_users(lobby) and not username in model.modelHandler.getter.lobby_challenges(lobby):
            print("Opponent found!")
            self.socket.write_message(protocol.gameprotocol.challenge_sent(username, opponent))
            model.modelHandler.getter.lobby_user(opponent, lobby).write_message(protocol.gameprotocol.challenge_sent(username, opponent))
            model.modelHandler.setter.lobby_challenge(username, opponent, self, lobby)
            functions.append(functools.partial(threading.Thread(target=lobby.challenge_loop, args=(username, opponent)).start))
        functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby)) 
        print("returning functions 2")
        return functions
    
    def accept_challenge(self, challenger):
        functions = []
        lobby = controller.controller_module.lobby
        model.modelHandler.setter.acquire_lobby_lock(lobby)
        if model.modelHandler.getter.socket_in_lobby(lobby, self.socket):
            username = model.modelHandler.getter.lobby_username_by_socket(lobby, self.socket)
        else:
            functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby)) 
            return functions
        if model.modelHandler.getter.lobby_challenges(lobby)[challenger] == username:
            functions_out = model.modelHandler.getter.player_message_handler_from_lobby(lobby, challenger).player_connect(challenger, username)
            controller.controller_module.processFunctions(functions_out)
            functions_out = self.player_connect(username, challenger)
            controller.controller_module.processFunctions(functions_out)
            model.modelHandler.setter.remove_challenges_from_lobby(challenger, username, lobby)
            model.modelHandler.setter.del_lobby_user(username, lobby)
            model.modelHandler.setter.del_lobby_user(challenger, lobby)
            for user in model.modelHandler.getter.lobby_users(lobby):
                model.modelHandler.getter.lobby_users(lobby)[user].write_message(protocol.gameprotocol.lobby_disconnect(username))
                model.modelHandler.getter.lobby_users(lobby)[user].write_message(protocol.gameprotocol.lobby_disconnect(challenger)) 
        functions.append(functools.partial(model.modelHandler.setter.release_lobby_lock, lobby)) 
        return functions
    
