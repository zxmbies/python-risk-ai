import abc
import model.deck
import copy
import tornado.escape

MESSAGE_TYPE = "message_type"

MESSAGE_TYPE_LOBBY_CONNECT = "lobby_connect"
MESSAGE_TYPE_LOBBY_DISCONNECT = "lobby_disconnect"
MESSAGE_TYPE_CHALLENGE_USER ="challenge_user"
MESSAGE_TYPE_CHALLENGE_SENT = "challenge_sent"
MESSAGE_TYPE_ACCEPT_CHALLENGE = "accept_challenge"
MESSAGE_TYPE_JOIN_QUEUE = "join_queue"
MESSAGE_TYPE_CREATE_LOBBY = "create_lobby"
MESSAGE_TYPE_NEW_GAME = "new_game"
MESSAGE_TYPE_BEGIN_TURN = "begin_turn"
MESSAGE_TYPE_PLACE_SHIPS = "place_ships"
MESSAGE_TYPE_DRAW_CARD = "draw_card"
MESSAGE_TYPE_ERROR = "error"
MESSAGE_TYPE_PLAYER_CONNECT = "player_connect"
MESSAGE_TYPE_END_TURN = "end_turn"
MESSAGE_TYPE_END_SETUP = "end_setup"
MESSAGE_TYPE_DRAW_CARD = "draw_card"
MESSAGE_TYPE_PLAY_CARDS = "play_cards"
MESSAGE_TYPE_DECLARE_ATTACK = "declare_attack"
MESSAGE_TYPE_DEFEND_ROLL = "defend_roll"
MESSAGE_TYPE_RESOLVE_ATTACK = "resolve_attack"
MESSAGE_TYPE_MOVE_SHIPS = "move_ships"
MESSAGE_TYPE_PLAYER_RECONNECT = "player_reconnect"
MESSAGE_TYPE_GAME_WON = "game_won"
MESSAGE_TYPE_CHALLENGE_FAILED = "challenge_failed"
MESSAGE_TYPE_DISCONNECTED_USER = "disconnected_user"
MESSAGE_TYPE_REMOVE_CARD = "remove_card"
MESSAGE_TYPE_FORFEIT = "forfeit"
MESSAGE_TYPE_SEND_USERNAME = "send_username"

FIELD_ID_CARDS = "cards"
FIELD_ID_NUM_CARDS = "num_cards"
FIELD_ID_MAP = "map"
FIELD_ID_PLAYERS = "players"
FIELD_ID_PLAYER_ID = "player_id"
FIELD_ID_NUM_SHIPS = "num_ships"
FIELD_ID_NODE_ID = "node_id"
FIELD_ID_ERROR_TYPE = "error_type"
FIELD_ID_CARD = "card"
FIELD_ID_NUM_DICE = "num_dice"
FIELD_ID_START_NODE = "start_node"
FIELD_ID_END_NODE = "end_node"
FIELD_ID_ATTACKER_LOSSES = "attacker_losses"
FIELD_ID_DEFENDER_LOSSES = "defender_losses"
FIELD_ID_USERNAME = "username"
FIELD_ID_OPPONENT = "opponent"
FIELD_ID_CAPTURED = "captured"
FIELD_ID_USERNAMES = "usernames"

'''
Since these messages are atomic there is no need to wrap them
in a dictionary when sending them.
'''
MESSAGE_END_TURN = "end_turn"
MESSAGE_END_SETUP = "end_setup"
MESSAGE_TIMER_START = "timer_start"
MESSAGE_TIMER_PAUSE = "timer_pause"
MESSAGE_END_GAME = "end_game"

class MessageHandler:
    '''
    This class reads an incoming message from the client and calls an
    appropriate method, to be implemented in a subclass.
    '''
    __metaclass__ = abc.ABCMeta
    
    def handle_message(self, message):
        '''
        This method is called whenever a new message is received from the
        client. It calls an appropriate abstract method.
        '''
        if message[MESSAGE_TYPE] == MESSAGE_TIMER_START:
            return self.start_timer()
        elif message[MESSAGE_TYPE] == MESSAGE_END_TURN:
            return self.end_turn()
        else:
            message_type = message[MESSAGE_TYPE]
            if message_type == MESSAGE_TYPE_PLACE_SHIPS:
                return self.place_ships(message[FIELD_ID_NUM_SHIPS], message[FIELD_ID_NODE_ID])
            elif message_type == MESSAGE_TYPE_PLAY_CARDS:
                cards = tornado.escape.json_decode(message[FIELD_ID_CARDS])
                for c in cards:
                    cards[c] = tornado.escape.json_decode(cards[c])
                return self.play_cards([model.deck.Card(cards[c]["node_id"], cards[c]["card_type"])
                                 for c in cards])
            elif message_type == MESSAGE_TYPE_DECLARE_ATTACK:
                return self.declare_attack(message[FIELD_ID_NUM_DICE],
                                    message[FIELD_ID_START_NODE],
                                    message[FIELD_ID_END_NODE])
            elif message_type == MESSAGE_TYPE_DEFEND_ROLL:
                return self.defend_roll(message[FIELD_ID_NUM_DICE])
            elif message_type == MESSAGE_TYPE_MOVE_SHIPS:
                return self.move_ships(message[FIELD_ID_NUM_SHIPS],
                                message[FIELD_ID_START_NODE],
                                message[FIELD_ID_END_NODE])
            elif message_type == MESSAGE_TYPE_DEFEND_ROLL:
                return self.defend_roll(message[FIELD_ID_NUM_DICE])
            elif message_type == MESSAGE_TYPE_MOVE_SHIPS:
                return self.move_ships(message[FIELD_ID_NUM_SHIPS],
                            message[FIELD_ID_START_NODE],
                            message[FIELD_ID_END_NODE])
            elif message_type == MESSAGE_TIMER_START:
                return self.start_timer()
            elif message_type == MESSAGE_TYPE_PLAYER_RECONNECT:
                return self.player_reconnect(message[FIELD_ID_USERNAME])
            elif message_type == MESSAGE_TYPE_PLAYER_CONNECT:
                return self.player_connect(message[FIELD_ID_USERNAME], message[FIELD_ID_OPPONENT])
            elif message_type == MESSAGE_TYPE_LOBBY_CONNECT:
                return self.lobby_connect(message[FIELD_ID_USERNAME])
            elif message_type == MESSAGE_TYPE_CHALLENGE_USER:
                return self.challenge_user(message[FIELD_ID_USERNAME])
            elif message_type == MESSAGE_TYPE_ACCEPT_CHALLENGE:
                return self.accept_challenge(message[FIELD_ID_USERNAME])
            elif message_type == MESSAGE_TYPE_JOIN_QUEUE:
                return self.join_queue()
            elif message_type == MESSAGE_TYPE_FORFEIT:
                return self.forfeit()
            else:
                return []
 
    @abc.abstractmethod
    def place_ships(self, num_ships, node_id):
        pass
    
    @abc.abstractmethod
    def end_turn(self):
        pass
    
    @abc.abstractmethod
    def play_cards(self, cards):
        pass
    
    @abc.abstractmethod
    def declare_attack(self, num_dice, start_node, end_node):
        pass
    
    @abc.abstractmethod
    def defend_roll(self, num_dice):
        pass
    
    @abc.abstractmethod
    def move_ships(self, num_ships, start_node, end_node):
        pass
    
    @abc.abstractmethod
    def start_timer(self):
        pass
    
    @abc.abstractmethod
    def player_reconnect(self, username):
        pass

    @abc.abstractmethod
    def player_connect(self, username, opponent):
        pass
    
    @abc.abstractmethod
    def lobby_connect(self, username):
        pass

    @abc.abstractmethod
    def challenge_user(self, username):
        pass
    
    @abc.abstractmethod
    def accepct_challenge(self):
        pass

    @abc.abstractmethod
    def join_queue(self):
        pass

    @abc.abstractmethod
    def forfeit(self):
        pass

# =================================================================
# The following functions create dictionaries to send to the client
# =================================================================

def player_to_dict(player):
    tmp = copy.deepcopy(player).__dict__
    del tmp[FIELD_ID_CARDS]
    tmp[FIELD_ID_NUM_CARDS] = len(player.cards)
    return tmp

def map_to_dict(game_map):
    tmp = copy.deepcopy(game_map)
    for node_id, node in tmp.nodes.items():
        tmp.nodes[node_id].player = player_to_dict(tmp.nodes[node_id].player)
        tmp.nodes[node_id] = node.__dict__
    for area_id, area in tmp.areas.items():
        tmp.areas[area_id] = area.__dict__
    return tmp.__dict__

def new_game(game_map, players):
    return {MESSAGE_TYPE : MESSAGE_TYPE_NEW_GAME,
            FIELD_ID_MAP : map_to_dict(game_map),
            FIELD_ID_PLAYERS : [player_to_dict(p) for p in players]}
    
def begin_turn(player_id, players):
    return {MESSAGE_TYPE : MESSAGE_TYPE_BEGIN_TURN,
            FIELD_ID_PLAYER_ID : player_id,
            FIELD_ID_PLAYERS : [player_to_dict(p) for p in players]}

def place_ships(num_ships, node_id):
    return {MESSAGE_TYPE : MESSAGE_TYPE_PLACE_SHIPS,
            FIELD_ID_NUM_SHIPS : num_ships,
            FIELD_ID_NODE_ID : node_id}

def error(error_type):
    return {MESSAGE_TYPE : MESSAGE_TYPE_ERROR,
            FIELD_ID_ERROR_TYPE : error_type}

def player_connect(player_id):
    return {MESSAGE_TYPE : MESSAGE_TYPE_PLAYER_CONNECT,
            FIELD_ID_PLAYER_ID : player_id}

def end_turn():
    return {MESSAGE_TYPE : MESSAGE_TYPE_END_TURN}

def end_setup():
    return {MESSAGE_TYPE : MESSAGE_TYPE_END_SETUP}

def draw_card(card):
    return {MESSAGE_TYPE : MESSAGE_TYPE_DRAW_CARD,
            FIELD_ID_CARD : card.__dict__}
    
def play_cards(num_ships):
    return {MESSAGE_TYPE : MESSAGE_TYPE_PLAY_CARDS,
            FIELD_ID_NUM_SHIPS : num_ships}
    
def remove_card(card):
    return {MESSAGE_TYPE: MESSAGE_TYPE_REMOVE_CARD,
            FIELD_ID_CARD : card.__dict__}

def declare_attack(num_dice, start_node, end_node):
    return {MESSAGE_TYPE : MESSAGE_TYPE_DECLARE_ATTACK,
            FIELD_ID_NUM_DICE : num_dice,
            FIELD_ID_START_NODE : start_node,
            FIELD_ID_END_NODE : end_node}
    
def resolve_attack(attacker_losses, defender_losses, captured, num_dice):
    return {MESSAGE_TYPE : MESSAGE_TYPE_RESOLVE_ATTACK,
            FIELD_ID_ATTACKER_LOSSES : attacker_losses,
            FIELD_ID_DEFENDER_LOSSES : defender_losses,
            FIELD_ID_CAPTURED  : captured,
            FIELD_ID_NUM_DICE : num_dice}

def move_ships(num_ships, start_node, end_node):
    return {MESSAGE_TYPE : MESSAGE_TYPE_MOVE_SHIPS,
            FIELD_ID_NUM_SHIPS : num_ships,
            FIELD_ID_START_NODE : start_node,
            FIELD_ID_END_NODE : end_node}

def game_won(winning_player):
    return {MESSAGE_TYPE: MESSAGE_TYPE_GAME_WON,
            FIELD_ID_USERNAME: winning_player}

def player_reconnect(username):
    return {MESSAGE_TYPE: MESSAGE_TYPE_PLAYER_RECONNECT,
            FIELD_ID_USERNAME: username}

def disconnected_user(username):
    return {MESSAGE_TYPE: MESSAGE_TYPE_DISCONNECTED_USER,
            FIELD_ID_USERNAME: username}
    
def lobby_disconnect(username):
    return {MESSAGE_TYPE : MESSAGE_TYPE_LOBBY_DISCONNECT,
            FIELD_ID_USERNAME : username}

def join_queue():
    return {MESSAGE_TYPE : MESSAGE_TYPE_JOIN_QUEUE}

def lobby_connect(username):
    return {MESSAGE_TYPE : MESSAGE_TYPE_LOBBY_CONNECT,
            FIELD_ID_USERNAME : username}

def create_lobby(lobby_users):
    return {MESSAGE_TYPE : MESSAGE_TYPE_CREATE_LOBBY,
            FIELD_ID_USERNAMES : [username for username in lobby_users]}

def send_username(username):
    return {MESSAGE_TYPE : MESSAGE_TYPE_SEND_USERNAME,
            FIELD_ID_USERNAME: username}

def challenge_sent(username, opponent):
    return {MESSAGE_TYPE : MESSAGE_TYPE_CHALLENGE_SENT,
            FIELD_ID_USERNAME : username,
            FIELD_ID_OPPONENT : opponent}
    
def challenge_failed(username, opponent):
    return {MESSAGE_TYPE : MESSAGE_TYPE_CHALLENGE_FAILED}