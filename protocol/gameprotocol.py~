import abc
import deck

MESSAGE_TYPE = "message_type"

MESSAGE_TYPE_NEW_GAME = "new_game"
MESSAGE_TYPE_BEGIN_TURN = "begin_turn"
MESSAGE_TYPE_PLACE_SHIPS = "place_ships"
MESSAGE_TYPE_DRAW_CARD = "draw_card"
MESSAGE_TYPE_ERROR = "error"
MESSAGE_TYPE_PLAYER_CONNECT = "player_connect"
MESSAGE_TYPE_END_TURN = "end_turn"
MESSAGE_TYPE_END_SETUP = "end_setup"
MESSAGE_TYPE_DRAW_CARD = "draw_card"
MESSAGE_TYPE_PLAY_CARDS = "play_cards"
MESSAGE_TYPE_DECLARE_ATTACK = "declare_attack"
MESSAGE_TYPE_DEFEND_ROLL = "defend_roll"
MESSAGE_TYPE_RESOLVE_ATTACK = "resolve_attack"
MESSAGE_TYPE_MOVE_SHIPS = "move_ships"
MESSAGE_TYPE_PLAYER_RECONNECT = "player_reconnect"
MESSAGE_TYPE_GAME_WON = "game_won"
MESSAGE_TYPE_DISCONNECTED_USER = "disconnected_user"

FIELD_ID_CARDS = "cards"
FIELD_ID_NUM_CARDS = "num_cards"
FIELD_ID_MAP = "map"
FIELD_ID_PLAYERS = "players"
FIELD_ID_PLAYER_ID = "player_id"
FIELD_ID_NUM_SHIPS = "num_ships"
FIELD_ID_NODE_ID = "node_id"
FIELD_ID_ERROR_TYPE = "error_type"
FIELD_ID_CARD = "card"
FIELD_ID_NUM_DICE = "num_dice"
FIELD_ID_START_NODE = "start_node"
FIELD_ID_END_NODE = "end_node"
FIELD_ID_ATTACKER_LOSSES = "attacker_losses"
FIELD_ID_DEFENDER_LOSSES = "defender_losses"
FIELD_ID_USERNAME = "username"
FIELD_ID_OPPONENT = "opponent"

'''
MESSAGE = {...
'''

MESSAGE_TIMER_START = {MESSAGE_TYPE : "timer_start"}
MESSAGE_TIMER_PAUSE = {MESSAGE_TYPE : "timer_pause"}
MESSAGE_END_GAME = {MESSAGE_TYPE : "end_game"}

class MessageHandler:
    '''
    This class reads an incoming message from the client and calls an
    appropriate method, to be implemented in a subclass.
    '''
    __metaclass__ = abc.ABCMeta
    
    def handle_message(self, message):
        '''
        This method is called whenever a new message is received from the
        client. It calls an appropriate abstract method.
        '''
        message_type = message[MESSAGE_TYPE]
        if message_type == MESSAGE_TYPE_PLACE_SHIPS:
            self.place_ships(message[FIELD_ID_NUM_SHIPS], message[FIELD_ID_NODE_ID])
        elif message_type == MESSAGE_TYPE_END_TURN:
            self.end_turn()
        elif message_type == MESSAGE_TYPE_PLAY_CARDS:
            self.play_cards([deck.Card(c["node_id"], c["card_type"])
                             for c in message[FIELD_ID_CARDS]])
        elif message_type == MESSAGE_TYPE_DECLARE_ATTACK:
            self.declare_attack(message[FIELD_ID_NUM_DICE],
                                message[FIELD_ID_START_NODE],
                                message[FIELD_ID_END_NODE])
        elif message_type == MESSAGE_TYPE_DEFEND_ROLL:
            self.defend_roll(message[FIELD_ID_NUM_DICE])
        elif message_type == MESSAGE_TYPE_MOVE_SHIPS:
            self.move_ships(message[FIELD_ID_NUM_SHIPS],
                            message[FIELD_ID_START_NODE],
                            message[FIELD_ID_END_NODE])
        elif message_type == MESSAGE_TIMER_START:
            self.start_timer()
        elif message_type == MESSAGE_TYPE_PLAYER_RECONNECT:
            self.player_reconnect(message[FIELD_ID_USERNAME])
        elif message_type == MESSAGE_TYPE_PLAYER_CONNECT:
            self.player_connect(message[FIELD_ID_USERNAME], message[FIELD_ID_OPPONENT])
    
    @abc.abstractmethod
    def place_ships(self, num_ships, node_id):
        pass
    
    @abc.abstractmethod
    def end_turn(self):
        pass
    
    @abc.abstractmethod
    def play_cards(self, cards):
        pass
    
    @abc.abstractmethod
    def declare_attack(self, num_dice, start_node, end_node):
        pass
    
    @abc.abstractmethod
    def defend_roll(self, num_dice):
        pass
    
    @abc.abstractmethod
    def move_ships(self, num_ships, start_node, end_node):
        pass
    
    @abc.abstractmethod
    def start_timer(self):
        pass
    
    @abc.abstractmethod
    def player_reconnect(self, username):
        pass

    @abc.abstractmethod
    def player_connect(self, username, opponent):
        pass

# =================================================================
# The following functions create dictionaries to send to the client
# =================================================================

def player_to_dict(player):
    tmp = player.__dict__
    print(len(player.cards))
    del tmp[FIELD_ID_CARDS]
    tmp[FIELD_ID_NUM_CARDS] = len(player.cards)
    return tmp

def new_game(game_map, players):
    return {MESSAGE_TYPE : MESSAGE_TYPE_NEW_GAME,
            FIELD_ID_MAP : game_map.__dict__,
            FIELD_ID_PLAYERS : [player_to_dict(p) for p in players]}
    
def begin_turn(player_id, players):
    return {MESSAGE_TYPE : MESSAGE_TYPE_BEGIN_TURN,
            FIELD_ID_PLAYER_ID : player_id,
            FIELD_ID_PLAYERS : [player_to_dict(p) for p in players]}

def place_ships(num_ships, node_id):
    return {MESSAGE_TYPE : MESSAGE_TYPE_PLACE_SHIPS,
            FIELD_ID_NUM_SHIPS : num_ships,
            FIELD_ID_NODE_ID : node_id}

def error(error_type):
    return {MESSAGE_TYPE : MESSAGE_TYPE_ERROR,
            FIELD_ID_ERROR_TYPE : error_type}

def player_connect(player_id):
    return {MESSAGE_TYPE : MESSAGE_TYPE_PLAYER_CONNECT,
            FIELD_ID_PLAYER_ID : player_id}

def end_turn():
    return {MESSAGE_TYPE : MESSAGE_TYPE_END_TURN}

def end_setup():
    return {MESSAGE_TYPE : MESSAGE_TYPE_END_SETUP}

def draw_card(card):
    return {MESSAGE_TYPE : MESSAGE_TYPE_DRAW_CARD,
            FIELD_ID_CARD : card.__dict__}
    
def play_cards(num_ships):
    return {MESSAGE_TYPE : MESSAGE_TYPE_PLAY_CARDS,
            FIELD_ID_NUM_SHIPS : num_ships}

def declare_attack(num_dice, start_node, end_node):
    return {MESSAGE_TYPE : MESSAGE_TYPE_DECLARE_ATTACK,
            FIELD_ID_NUM_DICE : num_dice,
            FIELD_ID_START_NODE : start_node,
            FIELD_ID_END_NODE : end_node}
    
def resolve_attack(attacker_losses, defender_losses, captured):
    return {MESSAGE_TYPE : MESSAGE_TYPE_RESOLVE_ATTACK,
            FIELD_ID_ATTACKER_LOSSES : attacker_losses,
            FIELD_ID_DEFENDER_LOSSES : defender_losses}

def move_ships(num_ships, start_node, end_node):
    return {MESSAGE_TYPE : MESSAGE_TYPE_MOVE_SHIPS,
            FIELD_ID_NUM_SHIPS : num_ships,
            FIELD_ID_START_NODE : start_node,
            FIELD_ID_END_NODE : end_node}

def game_won(winning_player):
    return {MESSAGE_TYPE: MESSAGE_TYPE_GAME_WON,
            FIELD_ID_USERNAME: winning_player}

def player_reconnect(username):
    return {MESSAGE_TYPE: MESSAGE_TYPE_PLAYER_RECONNECT,
            FIELD_ID_USERNAME: username}

def disconnected_user(username):
    return {MESSAGE_TYPE: MESSAGE_DISCONNECTED_USER,
            FIELD_ID_USERNAME: username}
