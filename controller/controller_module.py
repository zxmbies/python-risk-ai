''' 
Created on Oct 24, 2013

@author: thedoctor
'''

import random
import threading
import protocol.msghandler
import model.lobby
import model.modelHandler
import tornado.escape
import routing_table
import time
import copy
import pymysql

socket_to_game = routing_table.Routing_Table()
player_to_game = routing_table.Routing_Table()

def WebSocketControl(socket, message):
    print("starting message")
    MH = protocol.msghandler.HandlerImplementation(socket)
    message = tornado.escape.json_decode(message)
    print("Message type:", message["message_type"])
    logicFunctions = MH.handle_message(message)
    if logicFunctions:
        processFunctions(logicFunctions)
    print("finishing message")
        
def processFunctions(logicFunctions):
    for function in logicFunctions:
        tempFunctions = function()
        if tempFunctions:
            processFunctions(tempFunctions)
        
def end_turn(player, game):
    '''draws a card from the deck for the player if they've captured a country this turn,
    moves to the next turn, and calls the player "start turn tasks"'''
    
    if player.captured_country is True:
        card = model.modelHandler.setter.draw_card(player, game)
        socket = model.modelHandler.getter.socket_by_username(game, player.player_name)
        socket.write_message(protocol.gameprotocol.draw_card(card))
        
    player_turn = next_player_turn(model.modelHandler.getter.players(game), model.modelHandler.getter.player_turn(game))
    model.modelHandler.setter.player_turn(game, player_turn)
        
    player = model.modelHandler.getter.player_by_number(game, player_turn)
    neutral = model.modelHandler.getter.player_by_number(game, 0)
    start_turn_tasks(player, neutral, model.modelHandler.getter.game_map(game))
    model.modelHandler.setter.turn_timer(game, False)
    for socket in model.modelHandler.getter.game_sockets(game):
        socket.write_message(protocol.gameprotocol.end_turn())
        socket.write_message(protocol.gameprotocol.begin_turn(player.player_id, model.modelHandler.getter.players(game)))
            
    return []

def next_player_turn(players, player_turn):
    '''moves to the next player turn'''
    player_turn = player_turn + 1
    if player_turn > len(players) - 1:
        player_turn = 0
    if players[player_turn].eliminated == True:
        return next_player_turn( players, player_turn)
    return player_turn
    
def start_turn_tasks(player, neutral, game_map):
    '''sets appropriate flags and unit numbers'''
    setup_turns = model.modelHandler.getter.setup_turns(player)
    if setup_turns > 0:
        setup_turns = setup_turns - 1
        model.modelHandler.setter.setup_turns(setup_turns)
        model.modelHandler.setter.reserved_ships(player, 2)
        model.modelHandler.setter.reserved_ships(neutral, 1)
    else:
        model.modelHandler.setter.can_play_cards(player, True)
        model.modelHandler.setter.captured_country(player, False)
        num_nodes = model.modelHandler.getter.num_nodes(game_map, player)
        reserved_ships =  max(3, int(num_nodes/3))
        reserved_ships += model.modelHandler.getter.area_bonus(game_map, player)
        model.modelHandler.setter.reserved_ships(player, reserved_ships)
        
    return []
def place_ships(game, node, player, num_ships):
    model.modelHandler.setter.place_ships(player, node, num_ships)
    for socket in model.modelHandler.getter.game_sockets(game):
        socket.write_message(protocol.gameprotocol.place_ships(num_ships, model.modelHandler.getter.node_id(node)))
    return []
        
def play_cards(game, player, card1, card2, card3, socket):
    card_ships = model.modelHandler.getter.card_value(game)
    print(card1, card2, card3)
    for card in player.cards:
        print (card)
    card_ships += model.modelHandler.getter.node_bonus(model.modelHandler.getter.game_map(game), player, card1, card2, card3)
    model.modelHandler.setter.remove_cards(player, card1, card2, card3, card_ships)
    model.modelHandler.setter.add_cards(model.modelHandler.getter.deck(game), card1, card2, card3)
    socket.write_message(protocol.gameprotocol.remove_card(card1))
    socket.write_message(protocol.gameprotocol.remove_card(card2))
    socket.write_message(protocol.gameprotocol.remove_card(card3))
    card_value = model.modelHandler.getter.card_value(game)
    if card_value < 12:
        card_value += 2
    elif card_value is 12:
        card_value += 3
    elif card_value > 12:
        card_value += 5
    model.modelHandler.setter.card_value(game, card_value)
    for socket in model.modelHandler.getter.game_sockets(game):
        socket.write_message(protocol.gameprotocol.play_cards(card_ships))
    return []
        
def auto_place_ships(game, player, game_map):
    random.seed()
    ships = model.modelHandler.getter.remaining_ships(player)
    nodes = model.modelHandler.getter.player_nodes(player, game_map)   
    for x in range(ships):
        node_number = random.randint(0, len(nodes)-1)
        model.modelHandler.setter.change_ships(nodes[node_number], 1)
        for socket in model.modelHandler.getter.game_sockets(game):
            socket.write_message(protocol.gameprotocol.place_ships(1, model.modelHandler.getter.node_id(nodes[node_number])))
    return []
            
def resolve_capture(game):
    declared_attack = model.modelHandler.getter.declared_attack(game)
    if declared_attack["CAPTURED"] == True:
        (move_units, start_node, end_node) = (declared_attack["ATTACK_DICE"], declared_attack["START_NODE"], declared_attack["END_NODE"])
        model.modelHandler.setter.change_ships(start_node, -move_units)
        model.modelHandler.setter.change_ships(end_node, move_units)
        model.modelHandler.setter.declared_attack(game, {"ATTACK_DICE": None, "START_NODE": None,
                                "END_NODE":  None, "CAPTURED": False})
        for socket in model.modelHandler.getter.game_sockets(game):
            socket.write_message(protocol.gameprotocol.move_ships(move_units, 
                                                                  model.modelHandler.getter.node_id(start_node), 
                                                                  model.modelHandler.getter.node_id(end_node)))
    return []
            
def declare_attack(game, player, num_dice, start_node, end_node, attacking_neutral):
    if (not attacking_neutral):
        model.modelHandler.setter.decrease_player_turn_time(player, time.time() - model.modelHandler.getter.game_turn_timer(game))
        model.modelHandler.setter.game_turn_timer(game, False)
        model.modelHandler.setter.game_response_timer(game, time.time())
        print(player.turn_time)
    for socket in model.modelHandler.getter.game_sockets(game):
        socket.write_message(protocol.gameprotocol.declare_attack(num_dice, model.modelHandler.getter.node_id(start_node), model.modelHandler.getter.node_id(end_node)))
    model.modelHandler.setter.declared_attack(game, {"ATTACK_DICE": num_dice, "START_NODE": start_node,
                                "END_NODE": end_node, "CAPTURED": False})
    if (attacking_neutral):
        print("preparing to resolve attack")
        resolve_attack(game, model.modelHandler.getter.player_by_number(game, 0), min(2, model.modelHandler.getter.num_ships(end_node)))
    return []
        
def resolve_attack(game, player, num_dice):
        '''This function is really long and does a lot. Should I consider breaking it up
        into sub functions? Anyway, I've added comments that explain each part.'''
        
        '''populate lists of dice rolls'''
        attack_rolls = []
        defend_rolls = []
        random.seed()
        for it in range(model.modelHandler.getter.declared_attack(game)["ATTACK_DICE"]):
            attack_rolls.append(random.randint(1,6))
        for it in range(num_dice):
            defend_rolls.append(random.randint(1,6))
        
        '''sort in descending order'''
        attack_rolls = sorted(attack_rolls)[::-1]
        defend_rolls = sorted(defend_rolls)[::-1]
        
        '''determine number of possible compares'''
        total_matches = min(len(attack_rolls), len(defend_rolls))
        
        '''determine who won each compare'''
        wins = {"ATTACKER": 0, "DEFENDER": 0}
        for pos in range(total_matches):
            if attack_rolls[pos] > defend_rolls[pos]:
                wins["ATTACKER"] = wins["ATTACKER"] + 1
            else:
                wins["DEFENDER"] = wins["DEFENDER"] + 1
                
        '''remove appropriate units from each node'''
        end_node = model.modelHandler.getter.declared_attack(game)["END_NODE"]
        model.modelHandler.setter.num_ships(end_node, model.modelHandler.getter.num_ships(end_node) - wins["ATTACKER"])
        start_node = model.modelHandler.getter.declared_attack(game)["START_NODE"]
        model.modelHandler.setter.num_ships(start_node, model.modelHandler.getter.num_ships(start_node) - wins["DEFENDER"])
        
        '''determine if country was captured and exchange it, then set up waiting for a move between those two nodes'''
        declared_attack = model.modelHandler.getter.declared_attack(game)
        if model.modelHandler.getter.num_ships(declared_attack["END_NODE"]) == 0:
            model.modelHandler.setter.declared_attack(game, {"ATTACK_DICE": declared_attack["ATTACK_DICE"], "START_NODE": declared_attack["START_NODE"],
                                                          "END_NODE": declared_attack["END_NODE"], "CAPTURED": True})
            model.modelHandler.setter.captured_country(model.modelHandler.getter.player_by_node(declared_attack["START_NODE"]), True)
            model.modelHandler.setter.node_player(declared_attack["END_NODE"], model.modelHandler.getter.player_by_node(declared_attack["START_NODE"]))
            
            '''in case that was the players last node eliminate them'''
            eliminate_players(game, model.modelHandler.getter.game_map(game))
            for socket in model.modelHandler.getter.game_sockets(game):
                socket.write_message(protocol.gameprotocol.resolve_attack(wins["DEFENDER"], wins["ATTACKER"],
                                                                          model.modelHandler.getter.declared_attack(game)["CAPTURED"], num_dice))
            
            '''in case a player was eliminated, check to see if the game has been won'''
            if model.modelHandler.getter.num_players_remaining(game) == 1:
                process_win(game, model.modelHandler.getter.winner_name(game))
                return []
        else:
            '''if it was not captured, still end attack'''
            #print(wins["ATTACKER"])
            #print(wins["DEFENDER"])
            model.modelHandler.setter.declared_attack(game, {"ATTACK_DICE": None, "START_NODE": None,
                                    "END_NODE": None, "CAPTURED": False})
            for socket in model.modelHandler.getter.game_sockets(game):
                socket.write_message(protocol.gameprotocol.resolve_attack(wins["DEFENDER"], wins["ATTACKER"],
                                                                          model.modelHandler.getter.declared_attack(game)["CAPTURED"], num_dice))
        '''return player to the turn timer'''
        model.modelHandler.setter.turn_timer(game, time.time())
        model.modelHandler.setter.response_timer(game, False)
        return []
def eliminate_players(game, game_map):
    '''circles through all players, and calls the player model function
    to check if they have nodes, and if not eliminate them'''
    for player in model.modelHandler.getter.players(game):
        if model.modelHandler.getter.num_nodes(game_map, player) == 0:
            model.modelHandler.setter.eliminated(player, True)
    return []
            
def process_win(game, winning_player_name):
    '''Ends the game and announces a winner.'''
    conn = pymysql.connect(host='localhost', user='risk', passwd='galaxyvincent14', db='galcon')
    cur = conn.cursor()
    cur.execute('UPDATE users SET wins = wins + 1 WHERE username = "'+str(winning_player_name)+'";')
    
    for socket in copy.copy(model.modelHandler.getter.game_sockets(game)):
        socket.write_message(protocol.gameprotocol.game_won(winning_player_name))
        socket_to_game.remove(socket)
        username = model.modelHandler.getter.username_by_socket(game, socket)
        model.modelHandler.setter.del_socket_to_username(game, socket)
        model.modelHandler.setter.del_game_socket(game, socket)
        
        model.modelHandler.setter.acquire_lobby_lock(lobby)
        socket.write_message(protocol.gameprotocol.create_lobby(model.modelHandler.getter.lobby_users(lobby)))
        for user in model.modelHandler.getter.lobby_users(lobby):
            model.modelHandler.getter.lobby_user(user, lobby).write_message(protocol.gameprotocol.lobby_connect(username))
        model.modelHandler.setter.lobby_user(username, socket, lobby)
        model.modelHandler.setter.release_lobby_lock(lobby)    
    for player in model.modelHandler.getter.players(game):
        if model.modelHandler.getter.player_name(player) != "Neutral":
            player_to_game.remove(model.modelHandler.getter.player_name(player))
            print ("removing", player, "from player_to_game")
            if model.modelHandler.getter.player_name(player) != winning_player_name:
                cur.execute('UPDATE users SET losses = losses + 1 WHERE username = "'+str(model.modelHandler.getter.player_name(player))+'";')
    for player in copy.deepcopy(model.modelHandler.getter.disconnected_players(game)):
        model.modelHandler.setter.del_disconnected_player(game, player)
    conn.commit()
    cur.close()
    conn.close()
    return []

def move_ships(game, start_node, end_node, num_ships, resolving_capture):
    model.modelHandler.setter.change_ships(start_node, -num_ships)
    model.modelHandler.setter.change_ships(end_node, num_ships)
    for socket in model.modelHandler.getter.game_sockets(game):
        socket.write_message(protocol.gameprotocol.move_ships(num_ships, model.modelHandler.getter.node_id(start_node), 
                                                              model.modelHandler.getter.node_id(end_node)))
    if resolving_capture:
        model.modelHandler.setter.declared_attack(game, {"ATTACK_DICE": None, "START_NODE": None,
                                "END_NODE": None, "CAPTURED": False})
    else:
        end_turn(model.modelHandler.getter.player_by_node(start_node), game)
    return []
    
def start_turn_timer(game, player):
    print("starting turn timer")
    model.modelHandler.setter.turn_timer(game, time.time())
    model.modelHandler.setter.player_turn_time(player, 30)
    return []
    
def player_reconnect(game, player, socket):
    time_gone = int(time.time() - model.modelHandler.getter.disconnected_players(game)[player])
    model.modelHandler.setter.decrease_disconnect_time(player, time_gone)
            
    model.modelHandler.setter.del_disconnected_player(game, player)
    model.modelHandler.setter.socket_to_username(game, socket, model.modelHandler.getter.player_name(player))
    socket_to_game.set(socket, game)
    model.modelHandler.setter.game_socket(game, socket)
            
    for socket in model.modelHandler.getter.game_sockets(game):
        socket.write_message(protocol.gameprotocol.player_reconnect(model.modelHandler.getter.player_name(player)))
    socket.write_message(protocol.gameprotocol.new_game(model.modelHandler.getter.game_map(game), model.modelHandler.getter.players(game)))
    for card in model.modelHandler.getter.cards(player):
        socket.write_message(protocol.gameprotocol.draw_card(card))
    return []

def connect_player(game, username, socket):
    model.modelHandler.setter.socket_to_username(game, socket, username)
    model.modelHandler.setter.game_socket(game, socket)
    socket_to_game.set(socket, game)
    for socket in model.modelHandler.getter.game_sockets(game):
        socket.write_message(protocol.gameprotocol.player_connect(username))
    model.modelHandler.setter.add_player(game, model.modelHandler.getter.make_player(game, username)) 
    if model.modelHandler.getter.num_players(game) == 3:
        model.modelHandler.setter.game_started(game, True)
        assign_nodes(game, model.modelHandler.getter.players(game))
        start_turn_tasks(model.modelHandler.getter.player_by_number(game, model.modelHandler.getter.player_turn(game)),
                          model.modelHandler.getter.player_by_number(game, 0),  model.modelHandler.getter.game_map(game))
        for socket in model.modelHandler.getter.game_sockets(game):
            socket.write_message(protocol.gameprotocol.new_game(model.modelHandler.getter.game_map(game), model.modelHandler.getter.players(game)))
            socket.write_message(protocol.gameprotocol.begin_turn(1, model.modelHandler.getter.players(game)))
    return []
            
def assign_nodes(game, players):
        nodes = [model.modelHandler.getter.node_by_node_id(game, node) for node in model.modelHandler.getter.nodes(game)]
        random.shuffle(nodes)
        i = 0
        for node in nodes:
            model.modelHandler.setter.node_player(node, players[i%3])
            i = i + 1
        return []

def forfeit(game, player):
    model.modelHandler.setter.eliminated(player, True)    
    if model.modelHandler.getter.num_players_remaining(game) == 1:
        process_win(game, model.modelHandler.getter.winner_name(game))
    return []

lobby = model.lobby.Lobby()
threading.Thread(target = lobby.match_players).start()
