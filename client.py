from socket import *

HOST = "localhost"
PORT = 5001

BUFFER_SIZE = 1024

playernum = -1
users = -1
s = socket(AF_INET, SOCK_STREAM)
s.connect((HOST, PORT))
data = s.recv(BUFFER_SIZE)
data = data.decode('utf-8')
print(data)
if (str(data) == "You are player 1. Select number of users."):
    playernum = 0
    stuff = input()
    s.send(stuff.encode('utf-8'))

while(data != 'done'):
    data = s.recv(BUFFER_SIZE)
    data = data.decode('utf-8')
    if (data != ''):
        print("received data:", data)
        if (users == -1 and data == "Invalid input" and playernum == 0):
            print("Invalid number of players, please try again.")
            stuff = input()
            s.send(stuff.encode('utf-8'))
        elif (users == -1):
            try:
                users = int(data)
            except ValueError:
                print("Something terrible happened.")
        elif (playernum == -1):
            try:
                playernum = int(data)
                print("You are player ", playernum+1, ".", sep = '')
            except ValueError:
                print("Something terrible happened.")
s.close()
