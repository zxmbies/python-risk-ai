import threading

class Routing_Table:

    def set (self, key, value):
        with self.lock: self.table[key] = value

    def get(self, key):
        with self.lock: return self.table[key]
        
    def iterate(self, key):
        with self.lock: return key in self.table

    def remove(self, key):
        with self.lock: del self.table[key]
    
    def iterate_and_get(self, key):
        with self.lock: return self.table[key] if key in self.table else False
        
    def __init__(self):
        self.table = {}
        self.lock = threading.Lock ()