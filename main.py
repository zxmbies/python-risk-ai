import controller.controller_module
import ws.wshandler
import tornado.web
import tornado.httpserver
from web import game
from web import login
from web import account
import threading
import uuid

application = tornado.web.Application([
    (r'/message', ws.wshandler.WSHandler),
    (r'/game', game.GameHandler),
    (r'/login', login.Login),
    (r'/account', account.Account)],
    cookie_secret = str(uuid.uuid4())
)

def iofunc():
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8080)
    tornado.ioloop.IOLoop.instance().start()

threading.Thread(target=iofunc).start()
input()
tornado.ioloop.IOLoop.instance().stop()
print("Why the hell do I have to run from the root directory?")

