from CDeck import *
from CWorld import *
from CPlayer import *
from random import *
from socket import *
from time import *

class CGame():
    def determine_players(self):
        self.num_players = (input("How many players? (2-6)\n"))
        try:
            self.num_players = int(self.num_players)
        except ValueError:
            print("Nonvalid input, please try again")
            self.determine_players()
        if (self.num_players > 1 and self.num_players < 7):
            return
        print("Nonvalid input, please try again.")
        self.determine_players()

    def add_players(self):
        for it in range(self.num_players):
            player = CPlayer()
            self.player_list.append(player)
        if self.num_players is 2:
            player = CPlayer()
            self.player_list.append(player)

    def first_picker(self):
        self.first_player = randint(1, self.num_players)

    def pick_country_force(self, unchosen_countries):
        country_name = input()
        country = 0
        for country_it in unchosen_countries:
            if (country_it.get_name() == country_name):
                country = country_it
        if (country is 0):
            print("Invalid selection, please try again.")
            return self.pick_country(unchosen_countries)
        return country
    
    def pick_country(self, unchosen_countries):
        country_name = input()
        if (country_name == 'done' or country_name == 'Done' or country_name == 'DONE' or\
            country_name == 'cancel' or country_name == 'Cancel' or country_name == 'CANCEL'):
            return 0
        country = 0
        for country_it in unchosen_countries:
            if (country_it.get_name() == country_name):
                country = country_it
        if (country is 0):
            print("Invalid selection, please try again.")
            return self.pick_country(unchosen_countries)
        return country
    
    def pick_country_altered(self, unchosen_countries, player_turn):
        country_name = input()
        if (country_name == 'done' or country_name == 'Done' or country_name == 'DONE' or\
            country_name == 'cancel' or country_name == 'Cancel' or country_name == 'CANCEL'):
            return 0
        country = 0
        for country_it in unchosen_countries:
            player_owns = False
            for country_it2 in self.player_list[player_turn].countries:
                if country_it2 == country_it:
                    player_owns = True
            if (country_it.get_name() == country_name and player_owns == False):
                country = country_it
        if (country is 0):
            print("Invalid selection, please try again.")
            return self.pick_country(unchosen_countries)
        return country

    def next_player(self, player_turn):
        player_turn = player_turn + 1
        if (player_turn is self.num_players):
            player_turn = 0
        if (self.player_list[player_turn].forfeit is True):
            self.next_player(player_turn)
        return player_turn

    def choose_countries_2_player(self, world, s, conn):
        BUFFER_SIZE = 1024
        unchosen_countries = []
        for continent_it in world.get_continents():
            for country_it in continent_it.get_countries():
                unchosen_countries.append(country_it)

        player_turn = self.first_player - 1
        self.num_players = 3

        while (len(unchosen_countries) > 0):
            sleep(.01)
            message = "pt" + str(player_turn)
            for it in range(self.num_players-1):
                conn[it].send(message.encode('utf-8')) 
            country_num = randint(0, len(unchosen_countries)-1)
            message = "pc" + str(country_num)
            sleep(.01)
            for it in range(self.num_players-1):
                conn[it].send(message.encode('utf-8'))
            country = unchosen_countries[country_num]
            unchosen_countries.pop(country_num)
            self.player_list[player_turn].add_country(country)
            country.add_units(1)
            print("Process country:", country.get_name())
            player_turn = self.next_player(player_turn)
        self.num_players = 2
        sleep(.02)
        if (player_turn == 2):
            player_turn = 0
        return player_turn
        
    def choose_countries(self, world, s, conn):
        BUFFER_SIZE = 1024
        if (self.num_players is 2):
            player_turn = self.choose_countries_2_player(world, s, conn)
            return player_turn
        
        unchosen_countries = []
        for continent_it in world.get_continents():
            for country_it in continent_it.get_countries():
                unchosen_countries.append(country_it)

        player_turn = self.first_player - 1
        while (len(unchosen_countries) > 0):
            sleep(.03)
            message = "pt" + str(player_turn)
            for it in range(self.num_players):
                conn[it].send(message.encode('utf-8'))
            data = conn[player_turn].recv(BUFFER_SIZE)
            sleep(.03)
            data = data.decode('utf-8')
            message = "pc" + data
            for it in range(self.num_players):
                if (it != player_turn):
                    conn[it].send(message.encode('utf-8'))
            country_num = int(data)
            for country_it in unchosen_countries:
                if (country_it.get_number() == country_num):
                    unchosen_countries.remove(country_it)
                    self.player_list[player_turn].add_country(country_it)
            player_turn = self.next_player(player_turn)
        return player_turn

    def setup_units_2_players(self, world, player_turn, s, conn):
        BUFFER_SIZE = 1024
        num_countries = 0
        for continent_it in world.get_continents():
            for country_it in continent_it.get_countries():
                num_countries = num_countries + 1
        turn_number = int(num_countries/self.num_players)
        total_turns = 50 - self.num_players*5
        print(self.num_players)
        while(turn_number < total_turns):
            sleep(.02)
            message = "pt" + str(player_turn)
            for it in range(self.num_players):
                print("sending", message)
                conn[it].send(message.encode('utf-8'))
            print("Waiting")
            data = conn[player_turn].recv(BUFFER_SIZE)
            data = data.decode('utf-8')
            print('received', data)
            sleep(.02)
            message = "pc" + data
            for it in range(self.num_players):
                if (it != player_turn):
                    conn[it].send(message.encode('utf-8'))
            country_num = int(data)
            for country_it in self.player_list[player_turn].countries:
                if (country_it.get_number() == country_num):
                    country_it.add_units(1)
            data = conn[player_turn].recv(BUFFER_SIZE)
            data = data.decode('utf-8')
            sleep(.02)
            message = "pc" + data
            print('received', data)
            for it in range(self.num_players):
                if (it != player_turn):
                    conn[it].send(message.encode('utf-8'))
            country_num = int(data)
            for country_it in self.player_list[player_turn].countries:
                if (country_it.get_number() == country_num):
                    country_it.add_units(1)
            data = conn[player_turn].recv(BUFFER_SIZE)
            data = data.decode('utf-8')
            sleep(.02)
            message = "pc" + data
            print('received', data)
            for it in range(self.num_players):
                if (it != player_turn):
                    conn[it].send(message.encode('utf-8'))
            country_num = int(data)
            for country_it in self.player_list[2].countries:
                if (country_it.get_number() == country_num):
                    country_it.add_units(1)
            player_turn = self.next_player(player_turn)
            if (player_turn+1 is self.first_player):
                turn_number = turn_number + 2
        

    def setup_units(self, world, player_turn, s, conn):
        BUFFER_SIZE = 1024

        if (self.num_players is 2):
            self.setup_units_2_players(world, player_turn, s, conn)
            return
        num_countries = 0
        for continent_it in world.get_continents():
            for country_it in continent_it.get_countries():
                num_countries = num_countries + 1
        turn_number = int(num_countries/self.num_players) - 1
        total_turns = 50 - self.num_players*5
        while(turn_number < total_turns):
            sleep(.02)
            message = "pt" + str(player_turn)
            for it in range(self.num_players):
                conn[it].send(message.encode('utf-8'))
            data = conn[player_turn].recv(BUFFER_SIZE)
            data = data.decode('utf-8')
            sleep(.02)
            message = "pc" + data
            for it in range(self.num_players):
                if (it != player_turn):
                    conn[it].send(message.encode('utf-8'))
            country_num = int(data)
            for country_it in self.player_list[player_turn].countries:
                if (country_it.get_number() == country_num):
                    country_it.add_units(1)
            player_turn = self.next_player(player_turn)
            if (player_turn+1 is self.first_player):
                turn_number = turn_number + 1

    def get_cards(self, length, player_turn):
        line = input()
        if ((line == 'done') or (line == 'Done') or (line == 'DONE')):
            return 0
        pos = line.find(" ")
        if (pos != -1):
            num1 = line[:pos]
            line = line[pos+1:]
            pos = line.find(" ")
            if (pos != -1):
                num2 = line[:pos]
                num3 = line[pos+1:]
        try:
            num1 = int(num1)
            num2 = int(num2)
            num3 = int(num3)
        except ValueError:
            print("Invalid input, please try again")
            return self.get_cards(length, player_turn)
        except NameError:
            print("Invalid input, please try again")
            return self.get_cards(length, player_turn)
        if (num1 > 0 and num1 <= length and num2 > 0 and num2 <= length and num3 > 0 and num3 <= length \
            and num1 is not num2 and num1 is not num3 and num2 is not num3):
            num1 = num1 - 1
            num2 = num2 - 1
            num3 = num3 - 1
            cards = []
            cards.append(self.player_list[player_turn].hand[num1])
            cards.append(self.player_list[player_turn].hand[num2])
            cards.append(self.player_list[player_turn].hand[num3])
        else:   
            print("Invalid input, please try again")
            return self.get_cards(length, player_turn)
        return cards
        
    def get_card_bonus(self, player_turn):
        print("Player", player_turn+1, ", cards in hand are:")
        card_num = 1
        for card_it in self.player_list[player_turn].hand:
            print(card_num, ":", card_it.get_name(), ",", card_it.get_unit_type())
            card_num = card_num + 1
        print("Select 3 card numbers seperated by spaces, or type done")
        cards = self.get_cards(len(self.player_list[player_turn].hand), player_turn)
        if (cards is 0):
            return 0
        card_type_amount = {'Infantry': 0, 'Calvary': 0, 'Artillery': 0, 'Wild': 0}
        for card_it in cards:
            card_type_amount[card_it.get_unit_type()] = card_type_amount[card_it.get_unit_type()] + 1
        if ((card_type_amount['Infantry'] is 3 or (card_type_amount['Infantry'] is 2 and card_type_amount['Wild'] is 1)) or\
            (card_type_amount['Calvary'] is 3 or (card_type_amount['Calvary'] is 2 and card_type_amount['Wild'] is 1)) or\
            (card_type_amount['Artillery'] is 3 or (card_type_amount['Artillery'] is 2 and card_type_amount['Wild'] is 1)) or\
            (card_type_amount['Infantry'] is 1 and card_type_amount['Calvary'] is 1 and card_type_amount['Artillery'] is 1) or\
            (card_type_amount['Infantry'] is 1 and card_type_amount['Calvary'] is 1 and card_type_amount['Wild'] is 1) or\
            (card_type_amount['Infantry'] is 1 and card_type_amount['Wild'] is 1 and card_type_amount['Artillery'] is 1) or\
            (card_type_amount['Wild'] is 1 and card_type_amount['Calvary'] is 1 and card_type_amount['Artillery'] is 1)):
            self.sets_turned_in = self.sets_turned_in + 1
            has_country = False
            val = 0
            for country_it in self.player_list[player_turn].countries:
                for card_it in cards:
                    if (card_it.get_name() is country_it.get_name()):
                        val = 2
            if (self.sets_turned_in <6):
                return (val + 2 + 2*sets_turned_in)
            return (val + 10 + (5 * (sets_turned_in - 5)))
        print("Invalid selection...")
        return self.get_card_bonus(player_turn)
            

    def get_num_units(self, world, player_turn):
        num_units = 3
        num_countries = 0
        for country_it in self.player_list[player_turn].countries:
            num_countries = num_countries + 1
        if (num_countries/3 > num_units):
            num_units = int(num_countries/3)
        for continent_it in world.get_continents():
            continent = []
            for country_it in continent_it.get_countries():
                continent.append(country_it)
            for country_it2 in self.player_list[player_turn].countries:
                try:
                    continent.remove(country_it2)
                except ValueError:
                    pass
            if (len(continent) is 0):
                num_units = num_units + continent_it.get_unitBonus()
        num_units = num_units + self.get_card_bonus(player_turn)
        return num_units
                        

                
    def place_units(self, world, player_turn):
        num_units = self.get_num_units(world, player_turn)
        while (num_units > 0):
            print("Player", player_turn+1, ", you have", num_units, "left to place")
            print("Select one of your countries to place a unit in.")
            for country_it in self.player_list[player_turn].countries:
                if (country_it is not self.player_list[player_turn].countries[-1]):
                    print(country_it.get_name(), country_it.get_units(), ",", end=' ')
                else:
                    print(country_it.get_name(), country_it.get_units())
            country = self.pick_country_force(self.player_list[player_turn].countries)
            country.add_units(1)
            num_units = num_units - 1

    def choose_dice(self, max_dice, player):
        if (self.player_list[player].forfeit == True):
            return max_dice
        dice = input()
        try:
            dice = int(dice)
        except ValueError:
            print("Invalid selection, please try again")
            self.choose_dice(max_dice, player)
        if (dice > 0 and dice <= max_dice):
            return dice

    def roll_dice(self, dice_a, dice_d):
        rolls_a = []
        rolls_d = []
        for it in range(dice_a):
            rolls_a.append(randint(1, 6))
        for it in range(dice_d):
            rolls_d.append(randint(1, 6))
        rolls_a.sort()
        rolls_a.reverse()
        print("Attacking player rolls:", end=' ')
        for dice_it in rolls_a:
              print(dice_it, end=' ')
        print("")
        rolls_d.sort()
        rolls_d.reverse()
        print("Defending player rolls:", end=' ')
        for dice_it in rolls_d:
              print(dice_it, end=' ')
        print("")
        compares = min(dice_a, dice_d)
        wins = {'a': 0, 'd': 0}
        for it in range(compares):
            if (rolls_a[it] > rolls_d[it]):
                wins['a'] = wins['a'] + 1
            else:
                wins['d'] = wins['d'] + 1
        return wins
    
    def capture_move(self, country_a, country_d, dice_a, player_turn, defending_player):
        num = input()
        try:
            num = int(num)
        except ValueError:
            print("Invalid input, please try again.")
            capture_move(country_a, country_d, dice_a, player_turn, defending_player)
        if (num >= dice_a and num < country_a.units):
            self.player_list[defending_player].countries.remove(country_d)
            self.player_list[player_turn].countries.append(country_d)
            country_a.add_units(-1 * num)
            country_d.add_units(num)
            return
        print("Invalid input, please try again.")
        self.capture_move(country_a, country_d, dice_a, player_turn, defending_player)
        
    def battle(self, country_a, player_turn, country_d):
        max_a = 3
        max_d = 2
        if (country_a.get_units() < 5):
            max_a = country_a.get_units() - 1
        if (country_d.get_units() < 3):
            max_d = country_d.get_units()
        defending_player = 0
        defending_counter = 0
        for player_it in self.player_list:
            for country_it in player_it.countries:
                if (country_d == country_it):
                    defending_player = defending_counter
            defending_counter = defending_counter+1
        print("Player", player_turn+1, "select how many dice to roll between 1 and", max_a)
        dice_a = self.choose_dice(max_a, player_turn)
        print("Player", defending_player+1, "select how many dice to roll between 1 and", max_d)
        dice_d = self.choose_dice(max_d, defending_player)
        wins = self.roll_dice(dice_a, dice_d)
        if (wins['a'] != 0):
            print("Defending country lost", wins['a'], "units.")
        if (wins['d'] != 0):
            print("Attacking country lost", wins['d'], "units.")
        country_d.add_units(-1 * wins['a'])
        country_a.add_units(-1 * wins['d'])
        if (country_d.get_units() == 0):
            print(country_d.get_name(), "Captured! Select number of units to move between", dice_a, "and", country_a.get_units()-1)
            self.capture_move(country_a, country_d, dice_a, player_turn, defending_player)
            return 1
        return 0


    def combat(self, world, player_turn, countries_captured):
        while True:
            print("Countries are: ")
            for country_it in self.player_list[player_turn].countries:
                if (country_it is not self.player_list[player_turn].countries[-1]):
                    print(country_it.get_name(), country_it.get_units(), ",", end=' ')
                else:
                    print(country_it.get_name(), country_it.get_units())
            print("Player", player_turn+1, ", select one of your countries to attack from, or type done.")
            country_a = self.pick_country(self.player_list[player_turn].countries)
            if (country_a == 0):
                return countries_captured
            if (country_a.get_units() == 1):
                print("Not enough units to attack")
                combat(world, player_turn, countries_captured)
            print("Neighbors are: ")
            for country_it in country_a.neighbors:
                player_owns = False
                for country_it2 in self.player_list[player_turn].countries:
                    if (country_it2 == country_it):
                        player_owns = True
                if (country_it != country_a.neighbors[-1] and player_owns == False):
                    print(country_it.get_name(), country_it.get_units(), ",", end=' ')
                elif (player_owns == False):
                    print(country_it.get_name(), country_it.get_units())
            print("Player", player_turn+1, ", select one of your neighbors to attack, or type cancel.")
            country_d = self.pick_country_altered(country_a.neighbors, player_turn)
            if (country_d == 0):
                return combat(world, player_turn, countries_captured)
            countries_captured = countries_captured + self.battle(country_a, player_turn, country_d)
        return countries_captured

    def remove_players(self):
        for player_it in self.player_list:
            if (len(player_it.countries) == 0):
                player_it.forfeit = True

    def forfeit(self):
        if (self.check_win() == True):
            return
        line = input()
        if (line == 'done' or line == 'Done' or line == 'DONE'):
            return
        pos = line.find(" ")
        if (pos != -1):
            num = line[:pos]
            try:
                num = int(num)
            except ValueError:
                print("Invalid input, please try again")
                self.forfeit()
            line = line[pos+1:]
            if (num > 0 and num <= self.num_players and self.player_list[num-1].forfeit == False):
                if (line == 'forfeits'):
                    self.player_list[num-1].forfeit = True
                    print("Player", num, "forfeits! Would any other players like to forfeit? Type '# forfeits' or done")
                    self.forfeit()
        print("Invalid input, please try again")
        self.forfeit()

    def check_win(self):
        players_left = 0
        for player_it in self.player_list:
            if (player_it.forfeit == False):
                players_left = players_left + 1
        return (players_left is 1)

    def move_number(self, country_a, country_d):
        num = input()
        try:
            num = int(num)
        except ValueError:
            print("Invalid input, please try again")
            self.move_number(country_a, country_d)
        if (num > 0 and num < country_a.get_units()):
            country_a.add_units(-1 * num)
            country_d.add_units(num)
            return
        print("Invalid input, please try again")
        self.move_number(country_a, country_d)

    def move_units(self, player_turn):
        print("Countries are: ")
        for country_it in self.player_list[player_turn].countries:
            if (country_it is not self.player_list[player_turn].countries[-1]):
                print(country_it.get_name(), country_it.get_units(), ",", end=' ')
            else:
                print(country_it.get_name(), country_it.get_units())
        print("Player", player_turn+1, ", select one of your countries to move from, or type done.")
        country_a = self.pick_country(self.player_list[player_turn].countries)
        if (country_a == 0):
             return
        if (country_a.get_units() == 1):
                print("Not enough units to move")
                self.move_units(player_turn)
        print("Neighbors are: ")
        for country_it in country_a.neighbors:
            player_owns = False
            for country_it2 in self.player_list[player_turn].countries:
                if (country_it2 == country_it):
                    player_owns = True
            if (country_it is not country_a.neighbors[-1] and player_owns == True):
                print(country_it.get_name(), country_it.get_units(), ",", end=' ')
            elif (player_owns == True):
                print(country_it.get_name(), country_it.get_units())
        print("Player", player_turn+1, ", select one of your neighbors to move to, or type cancel.")
        country_d = self.pick_country(country_a.neighbors)
        if (country_d == 0):
            self.move_units(player_turn)
        print("Select the number of units to move")
        self.move_number(country_a, country_d)
            
            
            

    def game_play(self, world, deck):
        game_won = False
        player_turn = self.first_player - 2
        num_countries = 0
        for continent_it in world.get_continents():
            for country_it in continent_it.get_countries():
                num_countries = num_countries + 1
        while (game_won is False):
            print("Would any players like to forfeit this round? Type '# forfeits' or done")
            self.forfeit()
            game_won = self.check_win()
            if (game_won == True):
                break
            self.place_units(world, player_turn)
            countries_captured = self.combat(world, player_turn, 0)
            self.remove_players()
            game_won = self.check_win()
            if (game_won == True):
                break
            self.move_units(player_turn)
            if (countries_captured > 0):
                card = deck.get_card()
                self.player_list[player_turn].add_card(card)
            player_turn = self.next_player(player_turn)
        winner = 1
        for player_it in self.player_list:
            if (player_it.forfeit == True):
                winner = winner +1
            else:
                print("Player", winner, "Wins!!!")
        return
                
    def __init__(self, deck, world):
        seed()
        self.num_players = -1
        self.first_player = -1
        self.player_list = []
        HOST = "localhost"
        PORT = 5009
        BUFFER_SIZE = 1024

        s = socket(AF_INET, SOCK_STREAM)
        s.bind((HOST, PORT))
        s.listen(1)

        conn = []
        addr = []
        conn_temp, addr_temp = s.accept()
        conn.append(conn_temp)
        addr.append(addr_temp)
        print ("Connection address:", addr[0])
        message = "You are player 1. Select number of users."
        conn[0].send(message.encode("utf-8"))


        while (self.num_players < 2 or self.num_players > 6):
            data = conn[0].recv(BUFFER_SIZE)
            if (data != ''):
                try:
                    data = data.decode('utf-8')
                    self.num_players = int(data)
                    if (self.num_players < 2 or self.num_players > 6):
                        print("Invalid number")
                        message = "Invalid input"
                        conn[0].send(message.encode('utf-8'))
                    else:
                        message = str(self.num_players)
                        conn[0].send(message.encode('utf-8'))
                        self.add_players()
                        self.first_picker()
                        sleep(.02)
                        message = str(self.first_player)
                        conn[0].send(message.encode('utf-8'))
                except ValueError:
                    print("Unable to cast")
                    message = "Invalid input"
                    conn[0].send(message.encode('utf-8'))
            data = ''
        
        print("broke loop")
        i = 1
        while (i < self.num_players):
            print("Waiting for connection from", i+1,"player...")
            conn_temp, addr_temp = s.accept()
            conn.append(conn_temp)

            addr.append(addr_temp)
            print("Connection address:", addr[i])
            message = "Sucessfully connected to server!"
            conn[i].send(message.encode('utf-8'))
            sleep(.02)
            message = str(self.num_players)
            conn[i].send(message.encode('utf-8'))
            sleep(.02)
            message = str(i)
            conn[i].send(message.encode('utf-8'))
            sleep(.02)
            message = str(self.first_player)
            conn[i].send(message.encode('utf-8'))
            i = i + 1

        i = 0
        sleep(.02)
        player_turn = self.choose_countries(world, s, conn)
        sleep(.02)
        self.setup_units(world, player_turn, s, conn)
        sleep(.02)
        message = "done"
        while (i < self.num_players):
            conn[i].send(message.encode('utf-8'))
            i = i + 1

        i = 0
        while (i < self.num_players):
            conn[i].close()
            i = i + 1
