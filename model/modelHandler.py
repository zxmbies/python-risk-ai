'''
Created on Oct 24, 2013

@author: thedoctor
'''
import model.game
import model.player
import model.deck
import model.map
import copy

class get_class:
    
    def __init__(self):
        return
    
    def socket_by_username(self, game, username):
        for socket in game.game_to_sockets:
            if self.player_by_socket(game, socket).player_name is username:
                return socket
    
    def username_by_socket(self, game, socket):
        return game.socket_to_username[socket]
            
    def player_by_socket(self, game, socket):
        for player in game.players:
            if player.player_name == self.username_by_socket(game, socket):
                return player
    
    def players(self, game):
        return game.players
    
    def player_turn(self, game):
        return game.player_turn
    
    def player_by_number(self, game, player_number):
        return game.players[player_number]
    
    def game_map(self, game):
        return game.game_map
    
    def game_sockets(self, game):
        return game.game_to_sockets
    
    def setup_turns(self, player):
        return player.setup_turns
    
    def num_nodes(self, game_map, player):
        return len([game_map.nodes[node_id] for node_id in game_map.nodes if game_map.nodes[node_id].player == player])
                   
    def total_num_nodes(self, game_map):
        return len(game_map.nodes)
    
    def area_bonus(self, game_map, player):
        bonus = 0
        for area_id in game_map.areas:
            has_area = True
            for node_id in game_map.areas[area_id].nodes:
                if game_map.nodes[node_id].player != player:
                    has_area = False
            if has_area == True:
                bonus += game_map.areas[area_id].value
        return bonus
    
    def reserved_ships(self, player):
        return player.reserved_ships
    
    def node_by_node_id(self, game, node_id):
        return game.game_map.nodes[node_id]
    
    def player_id(self, player):
        return player.player_id
    
    def player_by_node(self, node):
        return node.player
    
    def num_cards(self, player):
        return len(player.cards)
    
    def captured_country(self, player):
        return player.captured_country
    
    def cards(self, player):
        return player.cards
    
    def card_value(self, game):
        return game.card_value
    
    def node_bonus(self, game_map, player, card1, card2, card3):
        (node1, node2, node3) = (False, False, False)
        if card1.node_id != -1: node1 = game_map.nodes[card1.node_id]
        if card2.node_id != -1: node2 = game_map.nodes[card2.node_id]
        if card3.node_id != -1: node3 = game_map.nodes[card3.node_id]
        if (node1 and node1.player == player) or (node2 and node2.player == player) or (node3 and node3.player == player):
            return 2
        return 0
    
    def deck(self, game):
        return game.deck
    
    def remaining_ships(self, player):
        ships = self.reserved_ships(player)
        setter.reserved_ships(player, 0)
        return ships
    
    def player_nodes(self, player, game_map):
        return [game_map.nodes[node_id] for node_id in game_map.nodes if game_map.nodes[node_id].player == player]

    def node_id(self, node):
        return node.node_id
    
    def card_node_id(self, card):
        return card.node_id

    def can_play_cards(self, player):
        return player.can_play_cards    
    
    def declared_attack(self, game):
        return game.declared_attack
    
    def card_type(self, card):
        return card.card_type

    def num_ships(self, node):
        return node.num_ships
    
    def connections(self, node):
        return node.connections
    
    def game_turn_timer(self, game):
        return game.turn_timer
    
    def num_players_remaining(self, game):
        players_left = 0
        for player in game.players:
            if player.eliminated == False:
                players_left += 1
        return players_left

    def player_name(self, player):
        return player.player_name
    
    def winner_name(self, game):
        for player in game.players:
            if player.eliminated == False:
                return player.player_name
            
    def turn_timer(self, game):
        return game.turn_timer
    
    def player_by_username(self, game, username):
        for player in game.players:
            if player.player_name == username:
                return player
            
    def disconnected_players(self, game):
        return game.disconnected_players
    
    def num_players(self, game):
        return len(game.players)
    
    def make_player(self, game, username):
        return model.player.Player(0, self.num_players(game), username)
    
    def nodes(self, game):
        return game.game_map.nodes
    
    def lobby_users(self, lobby):
        return lobby.users
    
    def lobby_user(self, username, lobby):
        return lobby.users[username]
   
    def socket_in_lobby(self, lobby, socket):
        for user in lobby.users:
            if lobby.users[user] == socket:
                return True
        return False
    
    def lobby_username_by_socket(self, lobby, socket):
        for user in lobby.users:
            if lobby.users[user] == socket:
                return user
            
    def lobby_challenges(self, lobby):
        return lobby.challenges
    
    def lobby_queue(self, lobby):
        return lobby.queue
    
    def player_message_handler_from_lobby(self, lobby, username):
        return lobby.msghandler[username]
    
    def game_started(self, game):
        return game.started

class set_class:
    
    def __init__(self):
        return
    
    def acquire_lock(self, game):
        game.lock.acquire()
        
    def draw_card(self, player, game):
        card = game.deck.draw_card()
        player.cards.append(card)
        return card

        
    def release_lock(self, game):
        game.lock.release()

    def enqueu(self, game):
        game.msg_queue.enqueue()
    
    def dequeue(self, game):
        game.msg_queue.dequeue()
    
    def player_turn(self, game, player_number):
        game.player_turn = player_number
        
    def turn_timer(self, game, value):
        game.turn_timer = value
    
    def setup_turns(self, player, value):
        player.setup_turns = value
        
    def reserved_ships(self, player, value):
        player.reserved_ships = value
        
    def can_play_cards(self, player, value):
        player.can_play_cards = value
        
    def captured_country(self, player, value):
        player.captured_country = value

    def place_ships(self, player, node, num_ships):
        player.reserved_ships -= num_ships
        player.can_play_card = False
        node.num_ships += num_ships
        
    def remove_cards(self, player, card1, card2, card3, card_ships):
        player.cards.remove(card1)
        player.cards.remove(card2)
        player.cards.remove(card3)
        player.reserved_ships += card_ships
    
    def add_cards(self, deck, card1, card2, card3):
        deck.cards.append(card1)
        deck.cards.append(card2)
        deck.cards.append(card3)
        
    def card_value(self, game, card_value):
        game.card_value = card_value
        
    def change_ships(self, node, num_ships):
        node.num_ships += num_ships
        
    def declared_attack(self, game, value):
        game.declared_attack = value
        
    def player_turn_time(self, player, value):
        player.turn_time = value
        
    def decrease_player_turn_time(self, player, value):
        player.turn_time = player.turn_time - value
        
    def game_turn_timer(self, game, value):
        game.turn_timer = value
        
    def game_response_timer(self, game, value):
        game.response_timer = value
        
    def num_ships(self, node, value):
        node.num_ships = value
        
    def node_player(self, node, player):
        node.player = player
        
    def eliminated(self, player, value):
        player.eliminated = value
        
    def response_timer(self, game, value):
        game.response_timer = value

    def socket_to_username(self, game, socket, username):
        game.socket_to_username[socket] = username

    def del_socket_to_username(self, game, socket):
        del game.socket_to_username[socket]
        
    def disconnected_player(self, game, player, value):
        game.disconnected_players[player] = value
        
    def del_disconnected_player(self, game, player):
        del game.disconnected_players[player]
        
    def del_game_socket(self, game, socket):
        game.game_to_sockets.remove(socket)
        
    def decrease_disconnect_time(self, player, time):
        player.disconnect_time -= time
        
    def game_socket(self, game, socket):
        game.game_to_sockets.append(socket)
        
    def add_player(self, game, player):
        game.players += (player,)
        
    def build_world(self, username):
    
        NUMBER_OF_CARDS = 44
        NUMBER_OF_TERRITORIES = 42
        CARD_TYPES = 3 #does not include wild
        '''Takes the first user, and adds them/neutral,
        calls helper function bmff to construct map, compiles
        deck, creates game from above data.'''
    
        players_temp = ()
        players_temp += (model.player.Player(0),)
        players_temp[0].eliminated = True
        players_temp += (model.player.Player(0, 1, username),)
    
    
        file = open("mapstring.data", "r")
        line = file.readline()
        game_map = model.map.Map(line)
        cards_temp = ()
        for node_id in range(0,NUMBER_OF_TERRITORIES, CARD_TYPES):
            for inc in range(CARD_TYPES):
                cards_temp += (model.deck.Card(node_id + inc, inc + 1),)
        cards_temp += (model.deck.Card(-1,0), )
        cards_temp += (model.deck.Card(-1,0), )
    
        game_deck = model.deck.Deck()
        for card in range(NUMBER_OF_CARDS):
            game_deck.cards.append(cards_temp[card])

        built_game = model.game.Game(game_map, game_deck, players_temp)
        return built_game
    
    def acquire_lobby_lock(self, lobby):
        lobby.lock.acquire()
        
    def release_lobby_lock(self, lobby):
        lobby.lock.release()
    
    def lobby_user(self, user, socket, lobby):
        lobby.users[user] = socket
        
    def join_lobby_queue(self, username, msghandler, lobby):
        lobby.queue.append((username, msghandler))
        
    def lobby_challenge(self, username, opponent, msghandler, lobby):
        lobby.challenges[username] = opponent
        lobby.msghandler[username] = msghandler
        
    def remove_challenges_from_lobby(self, username1, username2, lobby):
        if username1 in lobby.challenges:
            del lobby.challenges[username1]
            del lobby.msghandler[username1]
        if username2 in lobby.challenges:
            del lobby.challenges[username2]
            del lobby.msghandler[username2]
        for challenger in copy.copy(lobby.challenges):
            if self.challenges[challenger] == username1:
                del lobby.challenges[challenger]
            if self.challenges[challenger] == username2:
                del lobby.challenges[challenger]
        to_remove = [queue_entry for queue_entry in lobby.queue if (queue_entry[0] == username1 or queue_entry[0] == username2)]
        for queue_entry in to_remove:
            lobby.queue.remove(queue_entry)
                
    def del_lobby_user(self, username, lobby):
        del lobby.users[username]
        
    def del_lobby_queue_entry(self, lobby, queue_entry):
        lobby.queue.remove(queue_entry)
        
    def game_started(self, game, value):
        game.started = value
        

getter = get_class()
setter = set_class()