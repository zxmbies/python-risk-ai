import tornado.escape
import random

class MapNode(object):
    
    def __init__(self, node_id, connections, name):
        self.node_id = node_id
        self.num_ships = 1
        self.player = None
        self.area = None
        self.connections = connections
        self.name = name
        
    def add_connection(self, node):
        self.connections.append(node.node_id)
        node.connections.append(self.node_id)
    
    def set_player(self, player):
        self.player = player

    def set_area(self, area):
        self.area = area.area_id
        area.nodes.append(self.node_id)
        
    def change_ships(self, num_units):
        self.num_ships += num_units
        
class Area(object):
    
    def __init__(self, area_id, value, nodes):
        self.area_id = area_id
        self.value = value
        self.nodes = nodes

class Map(object):
    
    
    def __init__(self, string):
        self.nodes = {}
        self.areas = {}
        string = tornado.escape.json_decode(string)
        [self.nodes.update({string["nodes"][node]["node_id"] : MapNode(string["nodes"][node]["node_id"], string["nodes"][node]["connections"], string["nodes"][node]["name"])}) for node in string["nodes"]]
        [self.areas.update({string["areas"][area]["area_id"] : Area(string["areas"][area]["area_id"], string["areas"][area]["value"], string["areas"][area]["nodes"])}) for area in string["areas"]]
        
    def add_node(self, node):
        self.nodes[node.node_id] = node

    def add_area(self, area):
        self.areas[area.area_id] = area
        
    def to_dict(self):
        return {"nodes":[self.connections[n].to_dict() 
                         for n in list(self.connections.keys())]}
    
    '''def get_player_nodes(self, player):
        
        return [self.nodes[node_id] for node_id in self.nodes if self.nodes[node_id].player == player]
    
    def player_has_zero_nodes(self, player):
        
        value = True
        for node_id in self.nodes:
            value = False if self.nodes[node_id].player == player else value
        return value
    
    def get_area_bonus(self, player):
        
        bonus = 0
        for area_id in self.areas:
            has_area = True
            for node_id in self.areas[area_id].nodes:
                if self.nodes[node_id].player != player:
                    has_area = False
            if has_area == True:
                bonus += self.areas[area_id].value
        return bonus
    
    def get_node_bonus(self, player, card1, card2, card3):
        
        (node1, node2, node3) = (False, False, False)
        if card1.node_id != -1: node1 = self.nodes[card1.node_id]
        if card2.node_id != -1: node2 = self.nodes[card2.node_id]
        if card3.node_id != -1: node3 = self.nodes[card3.node_id]
        if (node1 and node1.player == player) or (node2 and node2.player == player) or (node3 and node3.player == player):
            return 2
        return 0
    
    def assign_nodes(self, players):
        nodes = [self.nodes[node] for node in self.nodes]
        random.shuffle(nodes)
        i = 0
        for node in nodes:
            node.set_player(players[i%3])
            i = i + 1'''
