'''
Created on Sep 27, 2013

@author: thedoctor
'''
import threading
import copy
import protocol.gameprotocol
import model.modelHandler
import controller.controller_module
import time

class Lobby(object):
    
    def __init__(self):
        self.queue = []
        self.msghandler = {}
        self.lock = threading.Lock()
        self.users = {}
        self.challenges = {}
        
    '''def join_queue(self, username, messager):
        self.queue.append((username, messager))'''

    def match_players(self):
        while(True):
            while len(self.queue) < 2: pass
            self.lock.acquire()
            if len(self.queue) >= 2:
                player1 = self.queue.pop(0)
                player2 = self.queue.pop(0)
                model.modelHandler.setter.remove_challenges_from_lobby(player1, player2, self)
                del self.users[player1[0]]
                del self.users[player2[0]]
                for user in self.users:
                    self.users[user].write_message(protocol.gameprotocol.lobby_disconnect(player1[0]))
                    self.users[user].write_message(protocol.gameprotocol.lobby_disconnect(player2[0])) 
                functions = player1[1].player_connect(player1[0], player2[0])
                controller.controller_module.processFunctions(functions)
                functions = player2[1].player_connect(player2[0], player1[0])
                controller.controller_module.processFunctions(functions)
                print("Players successfully matched!")
            self.lock.release()
            
    '''def remove_players_from_challenges_unsafe(self, username1, username2):
        if username1 in self.challenges:
            del self.challenges[username1]
        if username2 in self.challenges:
            del username2
        for challenger in copy.copy(self.challenges):
            if self.challenges[challenger] == username1:
                del self.challenges[challenger]
            if self.challenges[challenger] == username2:
                del self.challenges[challenger]
        
    
    def add_user(self, username, socket):
        self.users[username] = socket
        
    def iterate_users(self, socket):
        for user in self.users:
            if self.users[user] == socket:
                return True
        return False

    def get_username_by_socket(self, socket):
        for user in self.users:
            if self.users[user] == socket:
                return user
      
    '''
    def challenge_loop(self, challenger, challengee):
        start = time.time()
        while (time.time() - start < 30): pass
        self.lock.acquire()
        if (challenger in self.challenges and self.challenges[challenger] == challengee and challenger in self.users):
            self.users[challenger].write_message(protocol.gameprotocol.challenge_failed())
            del self.challenges[challenger]
            del self.msghandler[challenger]
        self.lock.release()