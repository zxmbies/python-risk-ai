import random

class Card(object):
    
    WILD = 0
    FRIGATE = 1
    BATTLESHIP = 2
    CAPTIAL_SHIP = 3
    
    def __init__(self, node_id, card_type):
        self.node_id = node_id
        self.card_type = card_type

class Deck(object):
    
    def __init__(self):
        self.cards = []

    def draw_card(self):
        '''Gets a random card from deck'''
        random.seed()
        random.shuffle(self.cards)
        return self.cards.pop()

    def add_cards(self, card1, card2, card3):
        '''adds turned-in cards to deck'''
        self.cards.append(card1)
        self.cards.append(card2)
        self.cards.append(card3)