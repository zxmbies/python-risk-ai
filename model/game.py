import threading
import time
import model.modelHandler
import copy
#import utilities.cw
import utilities.gm
import random
import protocol.gameprotocol
import My_Queue
import controller.controller_module

class Game:
    
    def __init__(self, game_map, deck, players):
        self.game_map = game_map
        self.deck = deck
        self.players = players
        self.player_turn = 1
        self.declared_attack = {"ATTACK_DICE": None, "START_NODE": None,
                                "END_NODE": None, "CAPTURED": False}
        self.card_value = 4
        self.game_to_sockets = []
        self.socket_to_username = {}
        self.lock = threading.Lock()
        self.msg_queue = My_Queue.My_Queue()
        
        self.disconnected_players = {}
        self.turn_timer = False
        self.response_timer = False
        self.started = False
    
    def game_loop(self):
        '''this function, used as a thread and called when the game is created,
        keeps track of the three timers that are important to each game: how long
        a user has been disconnected, how long a user has spent on their turn,
        and how long a user has taken to respond to an attack. When the time has
        been exceeded, it takes the required action'''
        
        print("game loop started")
        while(len(self.game_to_sockets) != 0 or len(self.disconnected_players) != 0):
            #acquire lock to prevent double processing
            self.lock.acquire()
            
            #disconnected players check
            disconnected_players = copy.copy(self.disconnected_players)
            for player in disconnected_players:
                if time.time() - disconnected_players[player] > player.disconnect_time:
                    player.eliminated = True
                    if model.modelHandler.getter.num_players_remaining(self) == 0:
                        controller.controller_module.process_win(self, model.modelHandler.getter.winner_name(self))
                        return
                    del self.disconnected_players[player]
                    
            #turn timer check
            player = self.players[self.player_turn]
            if self.turn_timer and time.time() - self.turn_timer > player.turn_time:
                print (player.turn_time)
                random.seed()
                
                #if captured country but did not move units, auto-move the minimum number of units
                if self.declared_attack["CAPTURED"] is True:
                    (move_units, start_node, end_node) = (self.declared_attack["ATTACK_DICE"], self.declared_attack["START_NODE"], 
                                                        self.declared_attack["END_NODE"])
                    (start_node.num_ships, end_node.num_ships) = (start_node.num_ships - move_units, end_node.num_ships + move_units)
                    self.declared_attack = {"ATTACK_DICE": None, "START_NODE": None,
                                            "END_NODE": None, "CAPTURED": False}
                    for socket in self.game_to_sockets:
                        socket.write_message(protocol.gameprotocol.move_ships(move_units, start_node.node_id, end_node.node_id))
                
                #auto-play cards if player has 6 cards    
                if len(player.cards) is 6:
                    card1, card2, card3 = utilities.gm.get_match(player.cards())
                    controller.controller_module.play_cards(self, player, card1, card2, card3) 
            
                #auto-place ships if ships are unplaced
                controller.controller_module.auto_place_ships(self, player, self.game_map)
                neutral = self.players[0]
                controller.controller_module.auto_place_ships(self, neutral, self.game_map)
                controller.controller_module.end_turn(player, self)
                
                #remove turn_timer
                self.turn_timer = False
            
            #response timer checker
            if self.response_timer and time.time() - self.response_timer > 5:
                player = self.declared_attack["END_NODE"].player
                controller.controller_module.resolve_attack(self, player, 1)
                self.response_timer = False
            
            #release lock to allow new messages to be processed
            self.lock.release()
'''
    def auto_place_ships(self, player, game_map):
        
        
        random.seed()        
        ships = player.acquire_remaining_ships()
        nodes = game_map.get_player_nodes(player)   
        for x in range(ships):
            node_number = random.randint(0, len(nodes)-1)
            nodes[node_number].num_ships = nodes[node_number].num_ships + 1
            for socket in self.game_to_sockets:
                socket.write_message(protocol.gameprotocol.place_ships(1, nodes[node_number].node_id))
    
    def one_player_left(self):
        players_left = 0
        for player in self.players:
            if player.eliminated == False:
                players_left += 1
        return players_left == 1
                
    def find_winner_name(self):
        for player in self.players:
            if player.eliminated == False:
                return player.player_name
            
    def process_win(self, winning_player_name):
        for socket in self.game_to_sockets:
            socket.write_message(protocol.gameprotocol.game_won(winning_player_name))
            header.socket_to_game.remove(socket)
            del self.socket_to_username[socket]
            socket.close()
        for player in self.players:
            if player.player_name is not "Neutral":
                header.player_to_game.remove(player.player_name)
                      
    def end_turn(self, player):
        if player.captured_country is True:
            card = player.add_card(self.deck)
            socket = self.find_socket_by_username(player.player_name)
            socket.write_message(protocol.gameprotocol.draw_card(card))
        self.next_player_turn()
        player = self.players[self.player_turn]
        neutral = self.players[0]
        game_map = self.game_map
        player.start_turn_tasks(neutral, game_map)
        self.turn_timer = False
        for socket in self.game_to_sockets:
            socket.write_message(protocol.gameprotocol.MESSAGE_END_TURN)
            socket.write_message(protocol.gameprotocol.begin_turn(player.player_id, self.players))
        
    def next_player_turn(self):
        self.player_turn = self.player_turn + 1
        if self.player_turn > len(self.players) - 1:
            self.player_turn = 0
        if self.players[self.player_turn].eliminated == True:
            self.next_player_turn()

    def find_player_by_socket(self, socket):
        for player in self.players:
            if player.player_name == self.socket_to_username[socket]:
                return player
    
    def find_player_by_username(self, username):
        for player in self.players:
            if player.player_name == username:
                return player
    
    def find_socket_by_username(self, username):
        for socket in self.game_to_sockets:
            if self.find_player_by_socket(socket).player_name is username:
                return socket

    def play_cards(self, player, card1, card2, card3):
        card_ships = self.card_value
        card_ships += self.game_map.get_node_bonus(player, card1, card2, card3)
        player.remove_cards(card1, card2, card3, card_ships)
        self.deck.add_cards(card1, card2, card3)
        if self.card_value < 12:
            self.card_value += 2
        elif self.card_value is 12:
            self.card_value += 3
        elif self.card_value > 12:
            self.card_value += 5
        for socket in self.game_to_sockets:
            socket.write_message(protocol.gameprotocol.play_cards(card_ships))
    
    def resolve_attack(self, player, num_dice):
        
        attack_rolls = []
        defend_rolls = []
        random.seed()
        for it in range(self.declared_attack["ATTACK_DICE"]):
            attack_rolls.append(random.randint(1,6))
        for it in range(num_dice):
            defend_rolls.append(random.randint(1,6))
        
        attack_rolls = sorted(attack_rolls)[::-1]
        defend_rolls = sorted(defend_rolls)[::-1]
        
        total_matches = min(len(attack_rolls), len(defend_rolls))
        
        wins = {"ATTACKER": 0, "DEFENDER": 0}
        for pos in range(total_matches):
            if attack_rolls[pos] > defend_rolls[pos]:
                wins["ATTACKER"] = wins["ATTACKER"] + 1
            else:
                wins["DEFENDER"] = wins["DEFENDER"] + 1
                
        self.declared_attack["END_NODE"].num_ships = self.declared_attack["END_NODE"].num_ships - wins["ATTACKER"]
        self.declared_attack["START_NODE"].num_ships = self.declared_attack["START_NODE"].num_ships - wins["DEFENDER"]
        
        if self.declared_attack["END_NODE"].num_ships is 0:
            self.declared_attack["CAPTURED"] = True
            self.declared_attack["START_NODE"].player.captured_country = True
            self.declared_attack["END_NODE"].player = self.declared_attack["START_NODE"].player
            
            self.eliminate_players(self.game_map)
            for socket in self.game_to_sockets:
                socket.write_message(protocol.gameprotocol.resolve_attack(wins["DEFENDER"], wins["ATTACKER"],
                                                                          self.declared_attack["CAPTURED"]))
            
            if self.one_player_left():
                self.proccess_win(self.find_winner_name())
                return
        else:
            print(wins["ATTACKER"])
            print(wins["DEFENDER"])
            self.declared_attack = {"ATTACK_DICE": None, "START_NODE": None,
                                    "END_NODE": None, "CAPTURED": False}
            for socket in self.game_to_sockets:
                socket.write_message(protocol.gameprotocol.resolve_attack(wins["DEFENDER"], wins["ATTACKER"],
                                                                          self.declared_attack["CAPTURED"]))
        self.turn_timer = time.time()
        self.response_timer = False

    def place_ships(self, node, player, num_ships):
        node.change_ships(num_ships)
        player.remove_ships(num_ships)
        for socket in self.game_to_sockets:
            socket.write_message(protocol.gameprotocol.place_ships(num_ships, node.node_id))
    
    def resolve_capture(self):            
        if self.declared_attack["CAPTURED"] == True:
            (move_units, start_node, end_node) = (self.declared_attack["ATTACK_DICE"], self.declared_attack["START_NODE"], self.declared_attack["END_NODE"])
            start_node.change_ships(-move_units)
            end_node.change_ships(move_units)
            self.declared_attack = {"ATTACK_DICE": None, "START_NODE": None,
                                    "END_NODE":  None, "CAPTURED": False}
            for socket in self.game_to_sockets:
                socket.write_message(protocol.gameprotocol.move_ships(move_units, start_node.node_id, end_node.node_id))
    
    def declare_attack(self, player, num_dice, start_node, end_node, attacking_neutral):
        if (not attacking_neutral):
            player.set_turn_time(time.time() - self.turn_timer)
            self.turn_timer = False
            self.response_timer = time.time()
        for socket in self.game_to_sockets:
            socket.write_message(protocol.gameprotocol.declare_attack(num_dice, start_node.node_id, end_node.node_id))
        self.declared_attack = {"ATTACK_DICE": num_dice, "START_NODE": start_node,
                                "END_NODE": end_node, "CAPTURED": False}
        if (attacking_neutral):
            self.resolve_attack(self.players[0], min(2, end_node.num_ships))
            
    def move_ships(self, start_node, end_node, num_ships, resolving_capture):
        start_node.change_ships(-num_ships)
        end_node.change_ships(num_ships)
        for socket in self.game_to_sockets:
            socket.write_message(protocol.gameprotocol.move_ships(num_ships, start_node.node_id, end_node.node_id))
        if resolving_capture:
            self.declared_attack = {"ATTACK_DICE": None, "START_NODE": None,
                                "END_NODE": None, "CAPTURED": False}
        else:
            self.end_turn(start_node.player)
            
    def start_turn_timer(self, player):
            self.turn_timer = time.time()
            player.set_turn_time(30)
            
    def player_reconnect(self, player, socket):
            time_gone = int(time.time() - self.disconnected_players[player])
            player.decrease_disconnect_time(time_gone)
            
            del self.disconnected_players[player]
            self.socket_to_username[socket] = player.player_name
            header.socket_to_game.set(socket, self)
            self.game_to_sockets.append(socket)
            
            for socket in self.game_to_sockets:
                socket.write_message(protocol.gameprotocol.player_reconnect(player.player_name))
            socket.write_message(protocol.gameprotocol.new_game(self.game_map, self.players))
            for card in player.cards:
                socket.write_message(protocol.gameprotocol.draw_card(card))
            
    def connect_player(self, username, socket):
        self.socket_to_username[socket] = username
        self.game_to_sockets.append(socket)
        header.socket_to_game.set(socket, self)
        for socket in self.game_to_sockets:
            socket.write_message(protocol.gameprotocol.player_connect(username))
        self.players += (model.player.Player(0, len(self.players), username),)
        if len(self.players) is 3:
            self.game_map.assign_nodes(self.players)
            self.players[self.player_turn].start_turn_tasks(self.players[0],  self.game_map)
            for socket in self.game_to_sockets:
                socket.write_message(protocol.gameprotocol.new_game(self.game_map, self.players))
                socket.write_message(protocol.gameprotocol.begin_turn(1, self.players))
                
    def find_opponent(self, username):
        for player in self.players:
            if player.player_name != username and player.player_name != "Neutral":
                return player.player_name
'''
