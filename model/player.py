import random

class Player(object):
    
    def __init__(self, reserved_ships, player_id = 0, player_name = "Neutral"):
        '''
        Constructor
        player_id and player_name default to the neutral player
        '''
        self.cards = []
        self.reserved_ships = reserved_ships
        self.player_id = player_id
        self.player_name = player_name
        self.disconnect_time = 30
        self.turn_time = 30
        self.eliminated = False
        self.can_play_cards = False
        self.captured_country = False
        self.setup_turns = 0
    
'''
    def acquire_remaining_ships(self):
        ships = self.reserved_ships
        self.reserved_ships = 0
        return ships
    
    def check_and_eliminate(self, game_map):
        if game_map.player_has_zero_nodes(self):
            self.eliminated = True
    
    def add_card(self, deck):
        card = deck.draw_card()
        self.cards.append(card)
        return card
    
    def start_turn_tasks(self, neutral, game_map):
        if self.setup_turns > 0:
            self.setup_turns = self.setup_turns - 1
            self.set_resreved_ships(2)
            neutral.set_reserved_ships(1)
        else:
            self.can_play_cards = True
            self.captured_country = False
            num_nodes = len(game_map.get_player_nodes(self))
            self.reserved_ships = max(3, int(num_nodes/3))
            area_bonus_ships = game_map.get_area_bonus(self)
            self.reserved_ships += area_bonus_ships
            
    def set_reserved_ships(self, num_ships):
        self.reserved_ships = num_ships
        
    def remove_cards(self, card1, card2, card3, card_ships):
        self.cards.remove(card1)
        self.cards.remove(card2)
        self.cards.remove(card3)
        self.reserved_ships += card_ships
        
    def remove_ships(self, num_ships):
        self.reserved_ships -= num_ships
        self.can_play_card = False
        
    def set_turn_time(self, time):
        self.turn_time = time
        
    def decrease_disconnect_time(self, time):
        self.disconnect_time -= time
'''